<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/partials/global/footer.htm */
class __TwigTemplate_f3db711364e4ba7727f6596c0f1ec6f17af6b5d2fbb9da61899bc31e2ae40f9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer class=\"footer\">

    <div class=\"footer-top footer-top--bg\">
        <div class=\"grid-container footer-top-inner\">
            <div class=\"grid-x grid-margin-x small-up-1 medium-up-2 large-up-4 align-justify\">
                <div class=\"cell footer-widget footer-widget-about\">
                    <div class=\"footer-widget-content\">
                        <img src=\"";
        // line 8
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/vidi-group-logo-footer.png");
        echo "\" srcset=\"
                                        ";
        // line 9
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/vidi-group-logo-footer.png");
        echo " 1x, 
                                        ";
        // line 10
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/vidi-group-logo-footer@2x.png");
        echo " 2x,
                                        ";
        // line 11
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/vidi-group-logo-footer@3x.png");
        echo " 3x
                                        \" class=\"footer-widget__logo\" alt=\"Vidi Group logotyp\">
                        <div class=\"footer-widget-desc\">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus magnam eaque sunt provident maiores ipsum fugiat amet laboriosam neque ratione necessitatibus unde, aliquid dolorem libero tempora quae tenetur sint fugit?
                        </div>
                    </div>
                </div>
                <div class=\"cell footer-widget footer-widget-contact\">
                    <div class=\"footer-widget-content\">
                        <div class=\"short-spacer\"></div>
                        <h3 class=\"footer-widget__header\">Kontakt</h3>
                        <ul class=\"footer-widget-big-list\">
                            <li class=\"footer-widget-big__item\">
                                <svg class=\"svg-icon footer-widget-big__icon\"><use xlink:href=\"#icon-pin-line\"></use></svg>
                                VIDI GROUP Sp. Z o. o. Sp. k.
                                <br />Ignacego Łukasiewicza 8/1
                                <br />43-300, Bielsko-Biała
                                <br />
                                <br />NIP: 547-218-49-78
                                <br />REGON: 000000001
                            </li>
                            <li class=\"footer-widget-big__item\">
                                <svg class=\"svg-icon footer-widget-big__icon\"><use xlink:href=\"#icon-email2\"></use></svg>
                                <strong>Masz pytania?</strong>
                                <a href=\"mailto:kontakt@vidigroup.pl\">kontakt@vidigroup.pl</a>
                            </li>
                            <li class=\"footer-widget-big__item\">
                                <svg class=\"svg-icon footer-widget-big__icon\"><use xlink:href=\"#icon-phone1\"></use></svg>
                                <strong>Telefon/Fax:</strong>
                                <a href=\"tel:+48 33 482 10 67\">+48 (33) 482 10 67</a>
                                <a href=\"tel:+48 33 845 73 56\">+48 (33) 845 73 56</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class=\"cell footer-widget footer-widget-menu\">
                    <div class=\"footer-widget-content\">
                        <div class=\"short-spacer\"></div>
                        <h3 class=\"footer-widget__header\">Menu</h3>
                        <ul class=\"footer-widget__menu\">
                            <li>
                                <a href=\"";
        // line 52
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("flota");
        echo "\" class=\"footer-widget__menu-link\">Flota</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 55
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("o-firmie");
        echo "\" class=\"footer-widget__menu-link\">O firmie</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 58
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("zespol");
        echo "\" class=\"footer-widget__menu-link\">Zespół</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 61
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("dokumenty");
        echo "\" class=\"footer-widget__menu-link\">Dokumenty</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 64
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("kontakt");
        echo "\" class=\"footer-widget__menu-link\">Kontakt</a>
                            </li>
                            <li>
                                <a href=\"http://wszystkoociasteczkach.pl/po-co-sa-ciasteczka/\" target=\"_blank\" class=\"footer-widget__menu-link\">Polityka prywatności</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class=\"cell footer-widget footer-widget-fb\">
                    <div class=\"footer-widget-content\">
                        <div class=\"short-spacer\"></div>
                        <h3 class=\"footer-widget__header\">Usługi</h3>
                        <ul class=\"footer-widget__menu\">
                            <li>
                                <a href=\"";
        // line 78
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-ekspresowy");
        echo "\" class=\"footer-widget__menu-link\">Transport ekspresowy</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 81
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-targi");
        echo "\" class=\"footer-widget__menu-link\">Transport na targi</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 84
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-przesiedlenia");
        echo "\" class=\"footer-widget__menu-link\">Transport przesiedlenia</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 87
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-chlodniczy");
        echo "\" class=\"footer-widget__menu-link\">Transport towarów chłodniczych</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 90
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-adr");
        echo "\" class=\"footer-widget__menu-link\">Transport ADR</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 93
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-na-wiszaco");
        echo "\" class=\"footer-widget__menu-link\">Transport towarów na \"wisząco\"</a>
                            </li>
                        </ul>
                    </div>
            
                </div>
            </div>
            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell small-12 footer-top-cookies\">
                    Ta strona używa ciasteczek. Korzystając ze tej strony wyrażasz zgodę na używanie ciasteczek, zgodnie z aktualnymi ustawieniami przeglądarki.
                    <a href=\"http://wszystkoociasteczkach.pl/po-co-sa-ciasteczka/\" target=\"_blank\">Dowiedz się więcej »</a> 
                </div>
            </div>
        </div>
    </div>

    <div class=\"footer-bottom footer-bottom--bg\">
        <div class=\"grid-container footer-bottom-inner\">
            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell small-12 footer-bottom-copyright\">
                    &copy; ";
        // line 113
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " VIDI GROUP Sp. Z o. o. Sp. k. | Wszystkie prawa zastrzeżone.
                </div>
            </div>
        </div>
    </div>

</div>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/global/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 113,  155 => 93,  149 => 90,  143 => 87,  137 => 84,  131 => 81,  125 => 78,  108 => 64,  102 => 61,  96 => 58,  90 => 55,  84 => 52,  40 => 11,  36 => 10,  32 => 9,  28 => 8,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<footer class=\"footer\">

    <div class=\"footer-top footer-top--bg\">
        <div class=\"grid-container footer-top-inner\">
            <div class=\"grid-x grid-margin-x small-up-1 medium-up-2 large-up-4 align-justify\">
                <div class=\"cell footer-widget footer-widget-about\">
                    <div class=\"footer-widget-content\">
                        <img src=\"{{'assets/img/vidi-group-logo-footer.png'|theme }}\" srcset=\"
                                        {{'assets/img/vidi-group-logo-footer.png'|theme }} 1x, 
                                        {{'assets/img/vidi-group-logo-footer@2x.png'|theme }} 2x,
                                        {{'assets/img/vidi-group-logo-footer@3x.png'|theme }} 3x
                                        \" class=\"footer-widget__logo\" alt=\"Vidi Group logotyp\">
                        <div class=\"footer-widget-desc\">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus magnam eaque sunt provident maiores ipsum fugiat amet laboriosam neque ratione necessitatibus unde, aliquid dolorem libero tempora quae tenetur sint fugit?
                        </div>
                    </div>
                </div>
                <div class=\"cell footer-widget footer-widget-contact\">
                    <div class=\"footer-widget-content\">
                        <div class=\"short-spacer\"></div>
                        <h3 class=\"footer-widget__header\">Kontakt</h3>
                        <ul class=\"footer-widget-big-list\">
                            <li class=\"footer-widget-big__item\">
                                <svg class=\"svg-icon footer-widget-big__icon\"><use xlink:href=\"#icon-pin-line\"></use></svg>
                                VIDI GROUP Sp. Z o. o. Sp. k.
                                <br />Ignacego Łukasiewicza 8/1
                                <br />43-300, Bielsko-Biała
                                <br />
                                <br />NIP: 547-218-49-78
                                <br />REGON: 000000001
                            </li>
                            <li class=\"footer-widget-big__item\">
                                <svg class=\"svg-icon footer-widget-big__icon\"><use xlink:href=\"#icon-email2\"></use></svg>
                                <strong>Masz pytania?</strong>
                                <a href=\"mailto:kontakt@vidigroup.pl\">kontakt@vidigroup.pl</a>
                            </li>
                            <li class=\"footer-widget-big__item\">
                                <svg class=\"svg-icon footer-widget-big__icon\"><use xlink:href=\"#icon-phone1\"></use></svg>
                                <strong>Telefon/Fax:</strong>
                                <a href=\"tel:+48 33 482 10 67\">+48 (33) 482 10 67</a>
                                <a href=\"tel:+48 33 845 73 56\">+48 (33) 845 73 56</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class=\"cell footer-widget footer-widget-menu\">
                    <div class=\"footer-widget-content\">
                        <div class=\"short-spacer\"></div>
                        <h3 class=\"footer-widget__header\">Menu</h3>
                        <ul class=\"footer-widget__menu\">
                            <li>
                                <a href=\"{{ 'flota'|page }}\" class=\"footer-widget__menu-link\">Flota</a>
                            </li>
                            <li>
                                <a href=\"{{ 'o-firmie'|page }}\" class=\"footer-widget__menu-link\">O firmie</a>
                            </li>
                            <li>
                                <a href=\"{{ 'zespol'|page }}\" class=\"footer-widget__menu-link\">Zespół</a>
                            </li>
                            <li>
                                <a href=\"{{ 'dokumenty'|page }}\" class=\"footer-widget__menu-link\">Dokumenty</a>
                            </li>
                            <li>
                                <a href=\"{{ 'kontakt'|page }}\" class=\"footer-widget__menu-link\">Kontakt</a>
                            </li>
                            <li>
                                <a href=\"http://wszystkoociasteczkach.pl/po-co-sa-ciasteczka/\" target=\"_blank\" class=\"footer-widget__menu-link\">Polityka prywatności</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class=\"cell footer-widget footer-widget-fb\">
                    <div class=\"footer-widget-content\">
                        <div class=\"short-spacer\"></div>
                        <h3 class=\"footer-widget__header\">Usługi</h3>
                        <ul class=\"footer-widget__menu\">
                            <li>
                                <a href=\"{{ 'transport-ekspresowy'|page }}\" class=\"footer-widget__menu-link\">Transport ekspresowy</a>
                            </li>
                            <li>
                                <a href=\"{{ 'transport-targi'|page }}\" class=\"footer-widget__menu-link\">Transport na targi</a>
                            </li>
                            <li>
                                <a href=\"{{ 'transport-przesiedlenia'|page }}\" class=\"footer-widget__menu-link\">Transport przesiedlenia</a>
                            </li>
                            <li>
                                <a href=\"{{ 'transport-chlodniczy'|page }}\" class=\"footer-widget__menu-link\">Transport towarów chłodniczych</a>
                            </li>
                            <li>
                                <a href=\"{{ 'transport-adr'|page }}\" class=\"footer-widget__menu-link\">Transport ADR</a>
                            </li>
                            <li>
                                <a href=\"{{ 'transport-na-wiszaco'|page }}\" class=\"footer-widget__menu-link\">Transport towarów na \"wisząco\"</a>
                            </li>
                        </ul>
                    </div>
            
                </div>
            </div>
            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell small-12 footer-top-cookies\">
                    Ta strona używa ciasteczek. Korzystając ze tej strony wyrażasz zgodę na używanie ciasteczek, zgodnie z aktualnymi ustawieniami przeglądarki.
                    <a href=\"http://wszystkoociasteczkach.pl/po-co-sa-ciasteczka/\" target=\"_blank\">Dowiedz się więcej »</a> 
                </div>
            </div>
        </div>
    </div>

    <div class=\"footer-bottom footer-bottom--bg\">
        <div class=\"grid-container footer-bottom-inner\">
            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell small-12 footer-bottom-copyright\">
                    &copy; {{ \"now\"|date(\"Y\") }} VIDI GROUP Sp. Z o. o. Sp. k. | Wszystkie prawa zastrzeżone.
                </div>
            </div>
        </div>
    </div>

</div>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/global/footer.htm", "");
    }
}
