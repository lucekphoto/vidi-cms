<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/pages/flota.htm */
class __TwigTemplate_98ea2efdae53fb87f432965ee3ae82f5695c9c4be7a65c1f73f5bc31b19e46ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"grid-container full main-header\">

        <!-- Title bar only on \"small\" screens -->
        ";
        // line 5
        echo "
        <div class=\"top-nav show-for-large\">
    
            <div class=\"grid-container\">
                <div class=\"grid-x grid-margin-x\">
                    <div class=\"cell\">
    
                        <!-- Info bar -->
                        ";
        // line 14
        echo "    
                        <!-- Main navigation -->
                        <div class=\"main-nav show-for-large\">
                            <div class=\"main-nav__logo\">
                                <a href=\"/\" class=\"main-nav__logo-link\">
                                    <img src=\"assets/img/vidi-group-logo-header.png\" srcset=\"assets/img/vidi-group-logo-header.png 1x,
                                                                                 assets/img/vidi-group-logo-header@2x.png 2x,
                                                                                 assets/img/vidi-group-logo-header@3x.png 3x\" class=\"main-nav__logo-img\"
                                        alt=\"Vidi Group logotyp\">
                                </a>
                            </div>
                            <div class=\"main-nav__menu\">
                                <ul class=\"dropdown menu\" data-dropdown-menu>
                                    <li><a href=\"/\">Strona główna</a></li>
                                    <li class=\"is-dropdown-submenu-parent\">
                                        <a href=\"/#\">Usługi</a>
                                        <ul class=\"menu vertical\">
                                          <li><a href=\"/transport-ekspresowy.html\">Transport ekspresowy</a></li>
                                          <li><a href=\"/transport-targi.html\">Transport na targi</a></li>
                                          <li><a href=\"/transport-przesiedlenia.html\">Transport przesiedlenia</a></li>
                                          <li><a href=\"/transport-chlodniczy.html\">Transport towarów chłodniczych</a></li>
                                          <li><a href=\"/transport-adr.html\">Transport towarów niebezpiecznych</a></li>
                                          <li><a href=\"/transport-na-wiszaco.html\">Transport towarów \"na wisząco\"</a></li>
                                        </ul>
                                    </li>
                                    <li class=\"active\"><a href=\"/#\">Flota</a></li>
                                    <li class=\"is-dropdown-submenu-parent\">
                                        <a href=\"/o-firmie.html\">O firmie</a>
                                        <ul class=\"menu vertical\">
                                            <li><a href=\"/zespol.html\">Team</a></li>
                                            <li><a href=\"/dokumenty.html\">Dokumenty</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/#\">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        
        <div class=\"page-header\" style=\"background-image: url(assets/img/page-header/header-1.jpg)\">
            <div class=\"grid-container page-header-inner\">
                <div class=\"page-header-inner-content\">
                    <h1>Flota</h1>
                    <nav aria-label=\"Jesteś tutaj:\" role=\"navigation\" class=\"breadcrumbs-wrapper\">
                        <ul class=\"breadcrumbs\">
                            <li><a href=\"/\">Strona główna</a></li>
                            <li>Flota</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
    </div>
    
    <section class=\"section-page-content\">
        <div class=\"grid-container\">

            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell small-12 section-page-content-intro\">

                    <div class=\"short-spacer short-spacer--center\"></div>
                    <h2 class=\"section-title\">Grafiki naszych samochodów są dojebane!</h2>
                    <p>Nasza flota zbudowana jest w oparciu o krótko oraz długoterminowe kontrakty z naszymi przewoźnikami. Dobieramy ich starannie
                    wg wielu czynników takich jak stan techniczny środków transportu, wyposażenia samochodów oraz doświadczenia kierowców. Gwarancją
                    najlepszego rozwiązania transportowego jest nasze doświadczenie oraz świadomość aktualnej sytuacji rynkowej w branży TSL
                    (Transport - Spedycja - Logistyka). Starannie dobieramy środki transportu do danej trasy uwzględniając przede wszystkim potrzeby
                    klienta oraz możliwości transportowe naszych samochodów i predyspozycje kierowców jest to gwarancją naszego sukcesu, jeżeli
                    chodzi o transport konkurujemy z największymi udziałowcami rynku TSL, a jakość naszych usług daje nam przewagę!</p>

                    <ul class=\"tabs tabs-fleet\" id=\"tabsFleet\" data-tabs data-deep-link=\"true\" data-update-history=\"true\" data-deep-link-smudge=\"true\" data-deep-link-smudge=\"500\" >
                        <li class=\"tabs-title is-active\">
                            <a href=\"#panel1\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"";
        // line 92
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/small/bus_5ep_nosize.png");
        echo "\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus - 5 EP</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel2\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"";
        // line 99
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/small/bus_5ep_long_nosize.png");
        echo "\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus - 5 EP (XL)</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel3\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"";
        // line 106
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/small/bus_8ep_plandeka_nosize.png");
        echo "\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus plandeka - 8 EP</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel4\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"";
        // line 113
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/small/bus_8ep_plandeka_long_nosize.png");
        echo "\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus plandeka - 10 EP</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel5\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"";
        // line 120
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/small/bus_8ep_fridge_nosize.png");
        echo "\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus chłodnia - 8 EP</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel6\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"";
        // line 127
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/small/bus_5ep_fridge_nosize.png");
        echo "\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus chłodnia - 5 EP</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel7\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"";
        // line 134
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/small/bus_8ep_plandeka_lift_trolley_nosize.png");
        echo "\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus plandeka - 8 EP winda</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel8\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"";
        // line 141
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/small/bus_8ep_nosize.png");
        echo "\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus - 8 EP</div>
                            </a>
                        </li>
                    </ul>

                    <div class=\"tabs-content tabs-content-fleet\" data-tabs-content=\"tabsFleet\">

                        <div class=\"tabs-panel is-active\" id=\"panel1\">
                            <!-- Bus 5 EP -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5 fleet-car-img\">
                                        <img src=\"";
        // line 155
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/bus_5ep.png");
        echo "\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>13%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Idealny do transportu towarów o wysokiej wartości, transport ekspresowy, ADR, towar z przedziału wagowego: 1001-1200 kg
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7 fleet-car-details\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus - 5 EP</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej:
                                                    <strong>360 / 170 / 170 cm</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6 fleet-car-spec\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">wymiary opcjonalne: 360 / 170 / 190 cm</li>
                                                    <li class=\"icon-list__item\">szerokość w nadkolach 135 cm</li>
                                                    <li class=\"icon-list__item\">5 Europalet</li>
                                                    <li class=\"icon-list__item\">ładowność 1200 kg</li>
                                                    <li class=\"icon-list__item\">sztywna zabudowa</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem</li>
                                                    <li class=\"icon-list__item\">możliwość plombowania</li>
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6 fleet-car-spec\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa - 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">GPS</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"tabs-panel\" id=\"panel2\">
                            <!-- Bus 5 EP - Long -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"";
        // line 203
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/bus_5ep_long.png");
        echo "\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>2%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Idealny do transportu towarów o wysokiej wartości, transport ekspresowy, towarów ADR, towarów z przedziału wagowego 1001-1100 kg
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus - 5 EP (XL)</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>420 / 170 / 170 cm</strong>
                                                </div>
                                                <!-- <div class=\"fleet-car-header__size\">Wymiary części ładownej (opcjonalne):
                                                    <strong>420 / 170 (135 cm między nadkolami) / 170-190 cm</strong>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">wymiary opcjonalne: 420 / 170 / 190 cm</li>
                                                    <li class=\"icon-list__item\">5 Europalet</li>
                                                    <li class=\"icon-list__item\">ładowność 1100 kg</li>
                                                    <li class=\"icon-list__item\">sztywna zabudowa</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem</li>
                                                    <li class=\"icon-list__item\">możliwość plombowania</li>
                                                    
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa - 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">GPS</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"tabs-panel\" id=\"panel3\">
                            <!-- Bus plandeka - 8 EP -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"";
        // line 254
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/bus_8ep_plandeka.png");
        echo "\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>39%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Transport ekspresowy, transport rzeczy przesiedlenia, towarów ADR
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus plandeka - 8 EP</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>420 / 200 / 210 cm</strong>
                                                </div>
                                                <!-- <div class=\"fleet-car-header__size\">Wymiary części ładownej (opcjonalne):
                                                    <strong>420 / 200-210 / 210-250 cm</strong>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">wymiary opcjonalne: 420 / 220 / 250 cm</li>
                                                    <li class=\"icon-list__item\">8 Europalet</li>
                                                    <li class=\"icon-list__item\">kubatura 18,5-22 CBM</li>
                                                    <li class=\"icon-list__item\">ładowność 1000 kg</li>
                                                    <li class=\"icon-list__item\">plandeka</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem, bokiem, górą*</li>
                                                    
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa - 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">GPS</li>
                                                    <li class=\"icon-list__item\">Linka celna</li>
                                                    <li class=\"icon-list__item\">maty antypoślizgowe*</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"tabs-panel\" id=\"panel4\">
                            <!-- Bus plandeka - 10 EP -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"";
        // line 307
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/bus_8ep_plandeka_long.png");
        echo "\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>24%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Transport ekspresowy, transport rzeczy przesiedlenia, towarów ADR
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus plandeka - 10 EP</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>480 / 200 / 210 cm</strong>
                                                </div>
                                                <!-- <div class=\"fleet-car-header__size\">Wymiary części ładownej (opcjonalne):
                                                    <strong>480 / 200-210 / 210-250 cm</strong>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">wymiary opcjonalne: 480 / 220 / 250 cm</li>
                                                    <li class=\"icon-list__item\">10 Europalet</li>
                                                    <li class=\"icon-list__item\">kubatura 21,1-25 CBM</li>
                                                    <li class=\"icon-list__item\">ładowność 900 kg</li>
                                                    <li class=\"icon-list__item\">plandeka</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem, bokiem, górą*</li> 
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">GPS</li>
                                                    <li class=\"icon-list__item\">linka celna*</li>
                                                    <li class=\"icon-list__item\">maty antypoślizgowe*</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class=\"tabs-panel\" id=\"panel5\">
                            <!-- Bus chłodnia - 8 EP -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"";
        // line 359
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/bus_8ep_fridge.png");
        echo "\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>5%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Przeznaczony do transportu towarów w temperaturze kontrolowanej, leków, towarów ADR, transport ekspresowy
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus chłodnia - 8 EP</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>420 / 200 / 200 cm</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">8 Europalet</li>
                                                    <li class=\"icon-list__item\">ładowność 700 kg</li>
                                                    <li class=\"icon-list__item\">zabudowa izotermiczna</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem</li>
                                                    <li class=\"icon-list__item\">możliwość plombowania</li>
                                                    <li class=\"icon-list__item\">agregat chlodniczy: -25C / +25C</li>    
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">rejestrator temperatury</li>
                                                    <li class=\"icon-list__item\">drukarka do wydruku temperatury</li>
                                                    <li class=\"icon-list__item\">GDP</li>
                                                    <li class=\"icon-list__item\">maty antypoślizgowe*</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"tabs-panel\" id=\"panel6\">
                            <!-- Bus chłodnia - 5 EP -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"";
        // line 410
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/bus_5ep_fridge.png");
        echo "\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>2%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Przeznaczony do transportu towarów w temperaturze kontrolowanej, towarów ADR
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus chłodnia - 5 EP</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>360 / 170 / 170 cm</strong>
                                                </div>
                                                <!-- <div class=\"fleet-car-header__size\">Wymiary części ładownej (opcjonalne):
                                                    <strong>360 / 170 (135 cm między nadkolami) / 170 cm</strong>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">8 Europalet</li>
                                                    <li class=\"icon-list__item\">ładowność 700 kg</li>
                                                    <li class=\"icon-list__item\">zabudowa izotermiczna</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem</li>
                                                    <li class=\"icon-list__item\">możliwość plombowania</li>
                                                    <li class=\"icon-list__item\">agregat chlodniczy: -25C / +25C</li>
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">rejestrator temperatury</li>
                                                    <li class=\"icon-list__item\">drukarka do wydruku temperatury</li>
                                                    <li class=\"icon-list__item\">GDP</li>
                                                    <li class=\"icon-list__item\">maty antypoślizgowe*</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"tabs-panel\" id=\"panel7\">
                            <!-- Bus - 8 EP winda -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"";
        // line 463
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/bus_8ep_plandeka_lift_trolley.png");
        echo "\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>13%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Idealny do transportu towarów na targi, rzeczy przesiedlenia, odbiory z lotnisk oraz miejsc bez ramp załadunkowych oraz braku
                                            dostępu wózków widłowych, transport ekspresowy
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus plandeka - 8 EP (winda)</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>420 / 210 / 210 cm</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">wymiary opcjonalne: 450 / 220 / 250 cm</li>
                                                    <li class=\"icon-list__item\">8 Europalet</li>
                                                    <li class=\"icon-list__item\">kubatura 18,5 - 24,8 CMB</li>
                                                    <li class=\"icon-list__item\">ładowność 700 kg</li>
                                                    <li class=\"icon-list__item\">plandeka</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem, bokiem*, górą*</li>
                                                    <li class=\"icon-list__item\">winda samoładująca</li>    
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">wózek paletowy</li>
                                                    <li class=\"icon-list__item\">GPS</li>
                                                    <li class=\"icon-list__item\">linka celna*</li>
                                                    <li class=\"icon-list__item\">maty antypoślizgowe*</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class=\"tabs-panel\" id=\"panel8\">
                            <!-- Bus - 8 EP Box -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"";
        // line 516
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/bus_8ep.png");
        echo "\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>2%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Idealny do transportu towarów o wysokiej wartości, towarów na wisząco, ADR, transport ekspresowy
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus - 8 EP</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>420 / 200 / 200 cm</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">8 Europalet</li>
                                                    <li class=\"icon-list__item\">ładowność 700 kg</li>
                                                    <li class=\"icon-list__item\">sztywna zabudowa</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem</li>
                                                    <li class=\"icon-list__item\">możliwość plombowania</li>
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">GPS</li>
                                                    <li class=\"icon-list__item\">maty antypoślizgowe*</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                    <li class=\"icon-list__item\">drążki do transportu towarów \"na wisząco\"</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                          
                </div>
            </div>
            
        </div>
    </section>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/pages/flota.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  583 => 516,  527 => 463,  471 => 410,  417 => 359,  362 => 307,  306 => 254,  252 => 203,  201 => 155,  184 => 141,  174 => 134,  164 => 127,  154 => 120,  144 => 113,  134 => 106,  124 => 99,  114 => 92,  34 => 14,  24 => 5,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"grid-container full main-header\">

        <!-- Title bar only on \"small\" screens -->
        {# {{> title-bar}} #}

        <div class=\"top-nav show-for-large\">
    
            <div class=\"grid-container\">
                <div class=\"grid-x grid-margin-x\">
                    <div class=\"cell\">
    
                        <!-- Info bar -->
                        {# {{'{{> top-nav-info-bar}}'|theme }} #}
    
                        <!-- Main navigation -->
                        <div class=\"main-nav show-for-large\">
                            <div class=\"main-nav__logo\">
                                <a href=\"/\" class=\"main-nav__logo-link\">
                                    <img src=\"assets/img/vidi-group-logo-header.png\" srcset=\"assets/img/vidi-group-logo-header.png 1x,
                                                                                 assets/img/vidi-group-logo-header@2x.png 2x,
                                                                                 assets/img/vidi-group-logo-header@3x.png 3x\" class=\"main-nav__logo-img\"
                                        alt=\"Vidi Group logotyp\">
                                </a>
                            </div>
                            <div class=\"main-nav__menu\">
                                <ul class=\"dropdown menu\" data-dropdown-menu>
                                    <li><a href=\"/\">Strona główna</a></li>
                                    <li class=\"is-dropdown-submenu-parent\">
                                        <a href=\"/#\">Usługi</a>
                                        <ul class=\"menu vertical\">
                                          <li><a href=\"/transport-ekspresowy.html\">Transport ekspresowy</a></li>
                                          <li><a href=\"/transport-targi.html\">Transport na targi</a></li>
                                          <li><a href=\"/transport-przesiedlenia.html\">Transport przesiedlenia</a></li>
                                          <li><a href=\"/transport-chlodniczy.html\">Transport towarów chłodniczych</a></li>
                                          <li><a href=\"/transport-adr.html\">Transport towarów niebezpiecznych</a></li>
                                          <li><a href=\"/transport-na-wiszaco.html\">Transport towarów \"na wisząco\"</a></li>
                                        </ul>
                                    </li>
                                    <li class=\"active\"><a href=\"/#\">Flota</a></li>
                                    <li class=\"is-dropdown-submenu-parent\">
                                        <a href=\"/o-firmie.html\">O firmie</a>
                                        <ul class=\"menu vertical\">
                                            <li><a href=\"/zespol.html\">Team</a></li>
                                            <li><a href=\"/dokumenty.html\">Dokumenty</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/#\">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        
        <div class=\"page-header\" style=\"background-image: url(assets/img/page-header/header-1.jpg)\">
            <div class=\"grid-container page-header-inner\">
                <div class=\"page-header-inner-content\">
                    <h1>Flota</h1>
                    <nav aria-label=\"Jesteś tutaj:\" role=\"navigation\" class=\"breadcrumbs-wrapper\">
                        <ul class=\"breadcrumbs\">
                            <li><a href=\"/\">Strona główna</a></li>
                            <li>Flota</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
    </div>
    
    <section class=\"section-page-content\">
        <div class=\"grid-container\">

            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell small-12 section-page-content-intro\">

                    <div class=\"short-spacer short-spacer--center\"></div>
                    <h2 class=\"section-title\">Grafiki naszych samochodów są dojebane!</h2>
                    <p>Nasza flota zbudowana jest w oparciu o krótko oraz długoterminowe kontrakty z naszymi przewoźnikami. Dobieramy ich starannie
                    wg wielu czynników takich jak stan techniczny środków transportu, wyposażenia samochodów oraz doświadczenia kierowców. Gwarancją
                    najlepszego rozwiązania transportowego jest nasze doświadczenie oraz świadomość aktualnej sytuacji rynkowej w branży TSL
                    (Transport - Spedycja - Logistyka). Starannie dobieramy środki transportu do danej trasy uwzględniając przede wszystkim potrzeby
                    klienta oraz możliwości transportowe naszych samochodów i predyspozycje kierowców jest to gwarancją naszego sukcesu, jeżeli
                    chodzi o transport konkurujemy z największymi udziałowcami rynku TSL, a jakość naszych usług daje nam przewagę!</p>

                    <ul class=\"tabs tabs-fleet\" id=\"tabsFleet\" data-tabs data-deep-link=\"true\" data-update-history=\"true\" data-deep-link-smudge=\"true\" data-deep-link-smudge=\"500\" >
                        <li class=\"tabs-title is-active\">
                            <a href=\"#panel1\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"{{'assets/img/fleet/small/bus_5ep_nosize.png'|theme }}\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus - 5 EP</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel2\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"{{'assets/img/fleet/small/bus_5ep_long_nosize.png'|theme }}\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus - 5 EP (XL)</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel3\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"{{'assets/img/fleet/small/bus_8ep_plandeka_nosize.png'|theme }}\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus plandeka - 8 EP</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel4\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"{{'assets/img/fleet/small/bus_8ep_plandeka_long_nosize.png'|theme }}\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus plandeka - 10 EP</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel5\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"{{'assets/img/fleet/small/bus_8ep_fridge_nosize.png'|theme }}\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus chłodnia - 8 EP</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel6\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"{{'assets/img/fleet/small/bus_5ep_fridge_nosize.png'|theme }}\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus chłodnia - 5 EP</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel7\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"{{'assets/img/fleet/small/bus_8ep_plandeka_lift_trolley_nosize.png'|theme }}\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus plandeka - 8 EP winda</div>
                            </a>
                        </li>
                        <li class=\"tabs-title\">
                            <a href=\"#panel8\" class=\"tabs-title-inner-wrap\" aria-selected=\"true\">
                                <img src=\"{{'assets/img/fleet/small/bus_8ep_nosize.png'|theme }}\" class=\"tabs-img\" alt=\"\">
                                <div class=\"short-spacer short-spacer--center\"></div>
                                <div class=\"tabs-title-inner\">Bus - 8 EP</div>
                            </a>
                        </li>
                    </ul>

                    <div class=\"tabs-content tabs-content-fleet\" data-tabs-content=\"tabsFleet\">

                        <div class=\"tabs-panel is-active\" id=\"panel1\">
                            <!-- Bus 5 EP -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5 fleet-car-img\">
                                        <img src=\"{{'assets/img/fleet/bus_5ep.png'|theme }}\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>13%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Idealny do transportu towarów o wysokiej wartości, transport ekspresowy, ADR, towar z przedziału wagowego: 1001-1200 kg
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7 fleet-car-details\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus - 5 EP</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej:
                                                    <strong>360 / 170 / 170 cm</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6 fleet-car-spec\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">wymiary opcjonalne: 360 / 170 / 190 cm</li>
                                                    <li class=\"icon-list__item\">szerokość w nadkolach 135 cm</li>
                                                    <li class=\"icon-list__item\">5 Europalet</li>
                                                    <li class=\"icon-list__item\">ładowność 1200 kg</li>
                                                    <li class=\"icon-list__item\">sztywna zabudowa</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem</li>
                                                    <li class=\"icon-list__item\">możliwość plombowania</li>
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6 fleet-car-spec\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa - 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">GPS</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"tabs-panel\" id=\"panel2\">
                            <!-- Bus 5 EP - Long -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"{{'assets/img/fleet/bus_5ep_long.png'|theme }}\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>2%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Idealny do transportu towarów o wysokiej wartości, transport ekspresowy, towarów ADR, towarów z przedziału wagowego 1001-1100 kg
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus - 5 EP (XL)</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>420 / 170 / 170 cm</strong>
                                                </div>
                                                <!-- <div class=\"fleet-car-header__size\">Wymiary części ładownej (opcjonalne):
                                                    <strong>420 / 170 (135 cm między nadkolami) / 170-190 cm</strong>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">wymiary opcjonalne: 420 / 170 / 190 cm</li>
                                                    <li class=\"icon-list__item\">5 Europalet</li>
                                                    <li class=\"icon-list__item\">ładowność 1100 kg</li>
                                                    <li class=\"icon-list__item\">sztywna zabudowa</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem</li>
                                                    <li class=\"icon-list__item\">możliwość plombowania</li>
                                                    
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa - 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">GPS</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"tabs-panel\" id=\"panel3\">
                            <!-- Bus plandeka - 8 EP -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"{{'assets/img/fleet/bus_8ep_plandeka.png'|theme }}\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>39%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Transport ekspresowy, transport rzeczy przesiedlenia, towarów ADR
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus plandeka - 8 EP</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>420 / 200 / 210 cm</strong>
                                                </div>
                                                <!-- <div class=\"fleet-car-header__size\">Wymiary części ładownej (opcjonalne):
                                                    <strong>420 / 200-210 / 210-250 cm</strong>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">wymiary opcjonalne: 420 / 220 / 250 cm</li>
                                                    <li class=\"icon-list__item\">8 Europalet</li>
                                                    <li class=\"icon-list__item\">kubatura 18,5-22 CBM</li>
                                                    <li class=\"icon-list__item\">ładowność 1000 kg</li>
                                                    <li class=\"icon-list__item\">plandeka</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem, bokiem, górą*</li>
                                                    
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa - 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">GPS</li>
                                                    <li class=\"icon-list__item\">Linka celna</li>
                                                    <li class=\"icon-list__item\">maty antypoślizgowe*</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"tabs-panel\" id=\"panel4\">
                            <!-- Bus plandeka - 10 EP -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"{{'assets/img/fleet/bus_8ep_plandeka_long.png'|theme }}\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>24%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Transport ekspresowy, transport rzeczy przesiedlenia, towarów ADR
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus plandeka - 10 EP</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>480 / 200 / 210 cm</strong>
                                                </div>
                                                <!-- <div class=\"fleet-car-header__size\">Wymiary części ładownej (opcjonalne):
                                                    <strong>480 / 200-210 / 210-250 cm</strong>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">wymiary opcjonalne: 480 / 220 / 250 cm</li>
                                                    <li class=\"icon-list__item\">10 Europalet</li>
                                                    <li class=\"icon-list__item\">kubatura 21,1-25 CBM</li>
                                                    <li class=\"icon-list__item\">ładowność 900 kg</li>
                                                    <li class=\"icon-list__item\">plandeka</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem, bokiem, górą*</li> 
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">GPS</li>
                                                    <li class=\"icon-list__item\">linka celna*</li>
                                                    <li class=\"icon-list__item\">maty antypoślizgowe*</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class=\"tabs-panel\" id=\"panel5\">
                            <!-- Bus chłodnia - 8 EP -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"{{'assets/img/fleet/bus_8ep_fridge.png'|theme }}\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>5%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Przeznaczony do transportu towarów w temperaturze kontrolowanej, leków, towarów ADR, transport ekspresowy
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus chłodnia - 8 EP</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>420 / 200 / 200 cm</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">8 Europalet</li>
                                                    <li class=\"icon-list__item\">ładowność 700 kg</li>
                                                    <li class=\"icon-list__item\">zabudowa izotermiczna</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem</li>
                                                    <li class=\"icon-list__item\">możliwość plombowania</li>
                                                    <li class=\"icon-list__item\">agregat chlodniczy: -25C / +25C</li>    
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">rejestrator temperatury</li>
                                                    <li class=\"icon-list__item\">drukarka do wydruku temperatury</li>
                                                    <li class=\"icon-list__item\">GDP</li>
                                                    <li class=\"icon-list__item\">maty antypoślizgowe*</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"tabs-panel\" id=\"panel6\">
                            <!-- Bus chłodnia - 5 EP -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"{{'assets/img/fleet/bus_5ep_fridge.png'|theme }}\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>2%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Przeznaczony do transportu towarów w temperaturze kontrolowanej, towarów ADR
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus chłodnia - 5 EP</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>360 / 170 / 170 cm</strong>
                                                </div>
                                                <!-- <div class=\"fleet-car-header__size\">Wymiary części ładownej (opcjonalne):
                                                    <strong>360 / 170 (135 cm między nadkolami) / 170 cm</strong>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">8 Europalet</li>
                                                    <li class=\"icon-list__item\">ładowność 700 kg</li>
                                                    <li class=\"icon-list__item\">zabudowa izotermiczna</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem</li>
                                                    <li class=\"icon-list__item\">możliwość plombowania</li>
                                                    <li class=\"icon-list__item\">agregat chlodniczy: -25C / +25C</li>
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">rejestrator temperatury</li>
                                                    <li class=\"icon-list__item\">drukarka do wydruku temperatury</li>
                                                    <li class=\"icon-list__item\">GDP</li>
                                                    <li class=\"icon-list__item\">maty antypoślizgowe*</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"tabs-panel\" id=\"panel7\">
                            <!-- Bus - 8 EP winda -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"{{'assets/img/fleet/bus_8ep_plandeka_lift_trolley.png'|theme }}\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>13%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Idealny do transportu towarów na targi, rzeczy przesiedlenia, odbiory z lotnisk oraz miejsc bez ramp załadunkowych oraz braku
                                            dostępu wózków widłowych, transport ekspresowy
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus plandeka - 8 EP (winda)</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>420 / 210 / 210 cm</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">wymiary opcjonalne: 450 / 220 / 250 cm</li>
                                                    <li class=\"icon-list__item\">8 Europalet</li>
                                                    <li class=\"icon-list__item\">kubatura 18,5 - 24,8 CMB</li>
                                                    <li class=\"icon-list__item\">ładowność 700 kg</li>
                                                    <li class=\"icon-list__item\">plandeka</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem, bokiem*, górą*</li>
                                                    <li class=\"icon-list__item\">winda samoładująca</li>    
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">wózek paletowy</li>
                                                    <li class=\"icon-list__item\">GPS</li>
                                                    <li class=\"icon-list__item\">linka celna*</li>
                                                    <li class=\"icon-list__item\">maty antypoślizgowe*</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class=\"tabs-panel\" id=\"panel8\">
                            <!-- Bus - 8 EP Box -->
                            <div class=\"fleet-car\">
                                <div class=\"grid-x grid-margin-x\">
                                    <div class=\"cell small-12 medium-5\">
                                        <img src=\"{{'assets/img/fleet/bus_8ep.png'|theme }}\" class=\"\" alt=\"\">
                                        <div class=\"fleet-car__share\">Udział we flocie: <span>2%</span></div>
                                        <div class=\"fleet-car__desc\">
                                            Idealny do transportu towarów o wysokiej wartości, towarów na wisząco, ADR, transport ekspresowy
                                        </div>
                                    </div>
                                    <div class=\"cell small-12 medium-7\">
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 fleet-car-header\">
                                                <h3 class=\"fleet-car-header__title\">Bus - 8 EP</h3>
                                                <div class=\"fleet-car-header__size\">Wymiary części ładownej (standardowe):
                                                    <strong>420 / 200 / 200 cm</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"grid-x grid-margin-x\">
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Specyfikacja:</li>
                                                    <li class=\"icon-list__item\">8 Europalet</li>
                                                    <li class=\"icon-list__item\">ładowność 700 kg</li>
                                                    <li class=\"icon-list__item\">sztywna zabudowa</li>
                                                    <li class=\"icon-list__item\">ładowanie tyłem</li>
                                                    <li class=\"icon-list__item\">możliwość plombowania</li>
                                                </ul>
                                            </div>
                                            <div class=\"cell small-12 medium-6\">
                                                <ul class=\"icon-list icon-list--small\">
                                                    <li class=\"icon-list__item icon-list__item-header\">Wyposażenie:</li>
                                                    <li class=\"icon-list__item\">gaśnica przeciwpożarowa 2 kg</li>
                                                    <li class=\"icon-list__item\">licencja ADR*</li>
                                                    <li class=\"icon-list__item\">GPS</li>
                                                    <li class=\"icon-list__item\">maty antypoślizgowe*</li>
                                                    <li class=\"icon-list__item\">pasy mocujące</li>
                                                    <li class=\"icon-list__item\">drążki do transportu towarów \"na wisząco\"</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                          
                </div>
            </div>
            
        </div>
    </section>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/pages/flota.htm", "");
    }
}
