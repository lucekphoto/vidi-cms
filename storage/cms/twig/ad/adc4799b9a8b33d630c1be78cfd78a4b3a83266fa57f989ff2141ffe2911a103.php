<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/pages/404.htm */
class __TwigTemplate_4fbb973eff453ad037ebd0e350b816557a08c06ba01b484f5a5814e08c2b311d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"jumbotron\">
    <div class=\"container\">
        <h1>Strony nie znaleziono</h1>
        <p>Przepraszamy, strona której szukasz nie istnieje.</p>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/pages/404.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"jumbotron\">
    <div class=\"container\">
        <h1>Strony nie znaleziono</h1>
        <p>Przepraszamy, strona której szukasz nie istnieje.</p>
    </div>
</div>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/pages/404.htm", "");
    }
}
