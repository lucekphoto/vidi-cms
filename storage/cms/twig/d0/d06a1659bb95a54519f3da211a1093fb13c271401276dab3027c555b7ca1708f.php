<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/partials/title-bar.htm */
class __TwigTemplate_28d0cd77d94c668c3c45ba2bcdb4faadb697331b3e155dd7c3299df52f62defa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Title bar only on \"small\" screens -->
<div class=\"title-bar hide-for-large\">
    <div class=\"title-bar-left\">
        <!-- This button fires OffCanvas Menu Panel -->
        <button class=\"menu-icon\" type=\"button\" data-open=\"offCanvasMenu\"></button>
    </div>
    <div class=\"title-bar-center\">
        <!-- Logo/brand name -->
        <span class=\"title-bar-title\">
            <a href=\"/\" class=\"\">
                <img src=\"assets/img/vidi-group-logo-header.png\" srcset=\"
                        assets/img/vidi-group-logo-header.png 1x, 
                        assets/img/vidi-group-logo-header@2x.png 2x,
                        assets/img/vidi-group-logo-header@3x.png 3x
                        \" class=\"main-nav__logo-img\" alt=\"Vidi Group logotyp\">
            </a>
        </span>
    </div>
    <div class=\"title-bar-right\">
        <!-- This button fires OffCanvas Language Panel -->
        <button class=\"globe-icon\" type=\"button\" data-open=\"offCanvasLang\">
            <svg class=\"svg-icon\">
                <use xlink:href=\"#icon-globe\"></use>
            </svg>
        </button>
        <!-- This button fires OffCanvas Contact Panel -->
        <button class=\"phone-icon\" type=\"button\" data-open=\"offCanvasContact\">
            <svg class=\"svg-icon\">
                <use xlink:href=\"#icon-phone-full\"></use>
            </svg>
        </button>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/title-bar.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Title bar only on \"small\" screens -->
<div class=\"title-bar hide-for-large\">
    <div class=\"title-bar-left\">
        <!-- This button fires OffCanvas Menu Panel -->
        <button class=\"menu-icon\" type=\"button\" data-open=\"offCanvasMenu\"></button>
    </div>
    <div class=\"title-bar-center\">
        <!-- Logo/brand name -->
        <span class=\"title-bar-title\">
            <a href=\"/\" class=\"\">
                <img src=\"assets/img/vidi-group-logo-header.png\" srcset=\"
                        assets/img/vidi-group-logo-header.png 1x, 
                        assets/img/vidi-group-logo-header@2x.png 2x,
                        assets/img/vidi-group-logo-header@3x.png 3x
                        \" class=\"main-nav__logo-img\" alt=\"Vidi Group logotyp\">
            </a>
        </span>
    </div>
    <div class=\"title-bar-right\">
        <!-- This button fires OffCanvas Language Panel -->
        <button class=\"globe-icon\" type=\"button\" data-open=\"offCanvasLang\">
            <svg class=\"svg-icon\">
                <use xlink:href=\"#icon-globe\"></use>
            </svg>
        </button>
        <!-- This button fires OffCanvas Contact Panel -->
        <button class=\"phone-icon\" type=\"button\" data-open=\"offCanvasContact\">
            <svg class=\"svg-icon\">
                <use xlink:href=\"#icon-phone-full\"></use>
            </svg>
        </button>
    </div>
</div>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/title-bar.htm", "");
    }
}
