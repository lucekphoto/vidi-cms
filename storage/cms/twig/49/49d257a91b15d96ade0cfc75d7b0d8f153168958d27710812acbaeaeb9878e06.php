<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/pages/dokumenty.htm */
class __TwigTemplate_3e2ab03e4dbee0214f16b628e19e3934fc134e426444450b30c224c5382d4e9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"grid-container full main-header\">

        <!-- Title bar only on \"small\" screens -->
        ";
        // line 5
        echo "
        <div class=\"top-nav show-for-large\">
    
            <div class=\"grid-container\">
                <div class=\"grid-x grid-margin-x\">
                    <div class=\"cell\">
    
                        <!-- Info bar -->
                        ";
        // line 14
        echo "    
                        <!-- Main navigation -->
                        <div class=\"main-nav show-for-large\">
                            <div class=\"main-nav__logo\">
                                <a href=\"/\" class=\"main-nav__logo-link\">
                                    <img src=\"assets/img/vidi-group-logo-header.png\" srcset=\"assets/img/vidi-group-logo-header.png 1x,
                                                                                 assets/img/vidi-group-logo-header@2x.png 2x,
                                                                                 assets/img/vidi-group-logo-header@3x.png 3x\" class=\"main-nav__logo-img\"
                                        alt=\"Vidi Group logotyp\">
                                </a>
                            </div>
                            <div class=\"main-nav__menu\">
                                <ul class=\"dropdown menu\" data-dropdown-menu>
                                    <li><a href=\"/\">Strona główna</a></li>
                                    <li class=\"is-dropdown-submenu-parent\">
                                        <a href=\"/#\">Usługi</a>
                                        <ul class=\"menu vertical\">
                                          <li><a href=\"/transport-ekspresowy.html\">Transport ekspresowy</a></li>
                                          <li><a href=\"/transport-targi.html\">Transport na targi</a></li>
                                          <li><a href=\"/transport-przesiedlenia.html\">Transport przesiedlenia</a></li>
                                          <li><a href=\"/transport-chlodniczy.html\">Transport towarów chłodniczych</a></li>
                                          <li><a href=\"/transport-adr.html\">Transport towarów niebezpiecznych</a></li>
                                          <li><a href=\"/transport-na-wiszaco.html\">Transport towarów \"na wisząco\"</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/flota.html\">Flota</a></li>
                                    <li class=\"is-dropdown-submenu-parent active\">
                                        <a href=\"/o-firmie.html\">O firmie</a>
                                        <ul class=\"menu vertical\">
                                            <li><a href=\"/zespol.html\">Team</a></li>
                                            <li><a href=\"/dokumenty.html\">Dokumenty</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/kontakt.html\">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        
        <div class=\"page-header\" style=\"background-image: url(assets/img/page-header/header-4.jpg)\">
            <div class=\"grid-container page-header-inner\">
                <div class=\"page-header-inner-content\">
                    <h1>Dokumenty</h1>
                    <nav aria-label=\"Jesteś tutaj:\" role=\"navigation\" class=\"breadcrumbs-wrapper\">
                        <ul class=\"breadcrumbs\">
                            <li><a href=\"/\">Strona główna</a></li>
                            <li><a href=\"/o-firmie.html\">O firmie</a></li>
                            <li>Dokumenty</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
    </div>

<section class=\"section-page-content\">
    <div class=\"grid-container\">

        <div class=\"grid-x grid-margin-x\">
            <div class=\"cell small-12\">
                <div class=\"short-spacer short-spacer--center\"></div>
                <h2 class=\"section-title\">Obadaj nasze dokumenty</h2>
                <p class=\"text-justify\">Jesteśmy do waszej dyspozycji 24/7 oferując usługi szyte na miarę. Zespół specjalistów codziennie dostarcza
                    Tobie najlepszych rozwiązań transportowych wspierając oraz rozwijając Twój biznes. Zgrany i silnie zmotywowany
                    team z jasno określonymi celami jest w ciągłej gotowości do działania.</p>

                <div class=\"grid-x grid-margin-x small-up-2 medium-up-5 doc-item-wrapper\">
                    <div class=\"cell doc-item\">
                        <a href=\"";
        // line 88
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/doc-licencja.pdf");
        echo "\">
                            <img src=\"";
        // line 89
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/licencja-prev.jpg");
        echo "\" srcset=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/licencja-prev.jpg");
        echo " 1x, ";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/licencja-prev@2x.jpg");
        echo " 2x, ";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/licencja-prev@3x.jpg");
        echo "\"
                                class=\"\" alt=\"licencja\">
                        </a>
                        <div class=\"short-spacer\"></div>
                        <h3 class=\"doc-item__title\">Licencja</h3>
                        <div class=\"doc-item__download\">
                            <svg class=\"svg-icon\">
                                <use xlink:href=\"#icon-doc-download\"></use>
                            </svg>
                            <a href=\"";
        // line 98
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/doc-licencja.pdf");
        echo "\" download=\"vidi-group-licencja\" class=\"doc-item__download-link\">Pobierz</a>
                        </div>
                    </div>
                    <div class=\"cell doc-item\">
                        <a href=\"";
        // line 102
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/doc-certyfikat-kompetencji.pdf");
        echo "\">
                            <img src=\"";
        // line 103
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/certyfikat-prev.jpg");
        echo "\" srcset=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/certyfikat-prev.jpg");
        echo " 1x, ";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/certyfikat-prev@2x.jpg");
        echo " 2x, ";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/certyfikat-prev@3x.jpg");
        echo "\"
                                class=\"\" alt=\"Certyfikat kompetencji zawodowych\">
                        </a>
                        <div class=\"short-spacer\"></div>
                        <h3 class=\"doc-item__title\">Certyfikat kompetencji zawodowych</h3>
                        <div class=\"doc-item__download\">
                            <svg class=\"svg-icon\">
                                <use xlink:href=\"#icon-doc-download\"></use>
                            </svg>
                            <a href=\"";
        // line 112
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/doc-certyfikat-kompetencji.pdf");
        echo "\" download=\"vidi-group-certyfikat-kompetencji-zawodowych\" class=\"doc-item__download-link\">Pobierz</a>
                        </div>
                    </div>
                    <div class=\"cell doc-item\">
                        <a href=\"";
        // line 116
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/doc-polisa-oc.pdf");
        echo "\">
                            <img src=\"";
        // line 117
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/polisa-prev.jpg");
        echo "\" srcset=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/polisa-prev.jpg");
        echo " 1x, ";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/polisa-prev@2x.jpg");
        echo " 2x, ";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/polisa-prev@3x.jpg");
        echo "\"
                                class=\"\" alt=\"Polisa ubezpieczenia OC\">
                        </a>
                        <div class=\"short-spacer\"></div>
                        <h3 class=\"doc-item__title\">Ubezpieczenie OC</h3>
                        <div class=\"doc-item__download\">
                            <svg class=\"svg-icon\">
                                <use xlink:href=\"#icon-doc-download\"></use>
                            </svg>
                            <a href=\"";
        // line 126
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/documents/doc-polisa-oc.pdf");
        echo "\" download=\"vidi-group-polisa-ubezpieczenia-oc\" class=\"doc-item__download-link\">Pobierz</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/pages/dokumenty.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 126,  172 => 117,  168 => 116,  161 => 112,  143 => 103,  139 => 102,  132 => 98,  114 => 89,  110 => 88,  34 => 14,  24 => 5,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"grid-container full main-header\">

        <!-- Title bar only on \"small\" screens -->
        {# {{> title-bar}} #}

        <div class=\"top-nav show-for-large\">
    
            <div class=\"grid-container\">
                <div class=\"grid-x grid-margin-x\">
                    <div class=\"cell\">
    
                        <!-- Info bar -->
                        {# {{> top-nav-info-bar}} #}
    
                        <!-- Main navigation -->
                        <div class=\"main-nav show-for-large\">
                            <div class=\"main-nav__logo\">
                                <a href=\"/\" class=\"main-nav__logo-link\">
                                    <img src=\"assets/img/vidi-group-logo-header.png\" srcset=\"assets/img/vidi-group-logo-header.png 1x,
                                                                                 assets/img/vidi-group-logo-header@2x.png 2x,
                                                                                 assets/img/vidi-group-logo-header@3x.png 3x\" class=\"main-nav__logo-img\"
                                        alt=\"Vidi Group logotyp\">
                                </a>
                            </div>
                            <div class=\"main-nav__menu\">
                                <ul class=\"dropdown menu\" data-dropdown-menu>
                                    <li><a href=\"/\">Strona główna</a></li>
                                    <li class=\"is-dropdown-submenu-parent\">
                                        <a href=\"/#\">Usługi</a>
                                        <ul class=\"menu vertical\">
                                          <li><a href=\"/transport-ekspresowy.html\">Transport ekspresowy</a></li>
                                          <li><a href=\"/transport-targi.html\">Transport na targi</a></li>
                                          <li><a href=\"/transport-przesiedlenia.html\">Transport przesiedlenia</a></li>
                                          <li><a href=\"/transport-chlodniczy.html\">Transport towarów chłodniczych</a></li>
                                          <li><a href=\"/transport-adr.html\">Transport towarów niebezpiecznych</a></li>
                                          <li><a href=\"/transport-na-wiszaco.html\">Transport towarów \"na wisząco\"</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/flota.html\">Flota</a></li>
                                    <li class=\"is-dropdown-submenu-parent active\">
                                        <a href=\"/o-firmie.html\">O firmie</a>
                                        <ul class=\"menu vertical\">
                                            <li><a href=\"/zespol.html\">Team</a></li>
                                            <li><a href=\"/dokumenty.html\">Dokumenty</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/kontakt.html\">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        
        <div class=\"page-header\" style=\"background-image: url(assets/img/page-header/header-4.jpg)\">
            <div class=\"grid-container page-header-inner\">
                <div class=\"page-header-inner-content\">
                    <h1>Dokumenty</h1>
                    <nav aria-label=\"Jesteś tutaj:\" role=\"navigation\" class=\"breadcrumbs-wrapper\">
                        <ul class=\"breadcrumbs\">
                            <li><a href=\"/\">Strona główna</a></li>
                            <li><a href=\"/o-firmie.html\">O firmie</a></li>
                            <li>Dokumenty</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
    </div>

<section class=\"section-page-content\">
    <div class=\"grid-container\">

        <div class=\"grid-x grid-margin-x\">
            <div class=\"cell small-12\">
                <div class=\"short-spacer short-spacer--center\"></div>
                <h2 class=\"section-title\">Obadaj nasze dokumenty</h2>
                <p class=\"text-justify\">Jesteśmy do waszej dyspozycji 24/7 oferując usługi szyte na miarę. Zespół specjalistów codziennie dostarcza
                    Tobie najlepszych rozwiązań transportowych wspierając oraz rozwijając Twój biznes. Zgrany i silnie zmotywowany
                    team z jasno określonymi celami jest w ciągłej gotowości do działania.</p>

                <div class=\"grid-x grid-margin-x small-up-2 medium-up-5 doc-item-wrapper\">
                    <div class=\"cell doc-item\">
                        <a href=\"{{ 'assets/img/documents/doc-licencja.pdf'|theme }}\">
                            <img src=\"{{ 'assets/img/documents/licencja-prev.jpg'|theme }}\" srcset=\"{{ 'assets/img/documents/licencja-prev.jpg'|theme }} 1x, {{ 'assets/img/documents/licencja-prev@2x.jpg'|theme }} 2x, {{ 'assets/img/documents/licencja-prev@3x.jpg'|theme }}\"
                                class=\"\" alt=\"licencja\">
                        </a>
                        <div class=\"short-spacer\"></div>
                        <h3 class=\"doc-item__title\">Licencja</h3>
                        <div class=\"doc-item__download\">
                            <svg class=\"svg-icon\">
                                <use xlink:href=\"#icon-doc-download\"></use>
                            </svg>
                            <a href=\"{{'assets/img/documents/doc-licencja.pdf'|theme }}\" download=\"vidi-group-licencja\" class=\"doc-item__download-link\">Pobierz</a>
                        </div>
                    </div>
                    <div class=\"cell doc-item\">
                        <a href=\"{{ 'assets/img/documents/doc-certyfikat-kompetencji.pdf'|theme }}\">
                            <img src=\"{{ 'assets/img/documents/certyfikat-prev.jpg'|theme }}\" srcset=\"{{ 'assets/img/documents/certyfikat-prev.jpg'|theme }} 1x, {{ 'assets/img/documents/certyfikat-prev@2x.jpg'|theme }} 2x, {{ 'assets/img/documents/certyfikat-prev@3x.jpg'|theme }}\"
                                class=\"\" alt=\"Certyfikat kompetencji zawodowych\">
                        </a>
                        <div class=\"short-spacer\"></div>
                        <h3 class=\"doc-item__title\">Certyfikat kompetencji zawodowych</h3>
                        <div class=\"doc-item__download\">
                            <svg class=\"svg-icon\">
                                <use xlink:href=\"#icon-doc-download\"></use>
                            </svg>
                            <a href=\"{{ 'assets/img/documents/doc-certyfikat-kompetencji.pdf'|theme }}\" download=\"vidi-group-certyfikat-kompetencji-zawodowych\" class=\"doc-item__download-link\">Pobierz</a>
                        </div>
                    </div>
                    <div class=\"cell doc-item\">
                        <a href=\"{{ 'assets/img/documents/doc-polisa-oc.pdf'|theme }}\">
                            <img src=\"{{ 'assets/img/documents/polisa-prev.jpg'|theme }}\" srcset=\"{{ 'assets/img/documents/polisa-prev.jpg'|theme }} 1x, {{ 'assets/img/documents/polisa-prev@2x.jpg'|theme }} 2x, {{ 'assets/img/documents/polisa-prev@3x.jpg'|theme }}\"
                                class=\"\" alt=\"Polisa ubezpieczenia OC\">
                        </a>
                        <div class=\"short-spacer\"></div>
                        <h3 class=\"doc-item__title\">Ubezpieczenie OC</h3>
                        <div class=\"doc-item__download\">
                            <svg class=\"svg-icon\">
                                <use xlink:href=\"#icon-doc-download\"></use>
                            </svg>
                            <a href=\"{{ 'assets/img/documents/doc-polisa-oc.pdf'|theme }}\" download=\"vidi-group-polisa-ubezpieczenia-oc\" class=\"doc-item__download-link\">Pobierz</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/pages/dokumenty.htm", "");
    }
}
