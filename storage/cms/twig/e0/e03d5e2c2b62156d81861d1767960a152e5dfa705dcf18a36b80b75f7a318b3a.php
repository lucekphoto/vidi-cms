<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/partials/top-nav-info-bar.htm */
class __TwigTemplate_0f7f0a5339fd2eff8f9ee153828cf3ea7be46d267b770ad5e4e19547804664e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Info bar -->
<div class=\"info-bar grid-x show-for-medium\">

    <ul class=\"cell auto info-bar__list\">
        <li class=\"info-bar__title\">Kontakt:</li>
        <li class=\"info-bar__item\">
            <svg class=\"svg-icon info-bar__icon\"><use xlink:href=\"#icon-phone1\"></use></svg>
            <a href=\"tel:+48 123 456 789\" class=\"info-bar__link\">+48 (33) 482 10 67</a>
        </li>
        <li class=\"info-bar__item\">
            <svg class=\"svg-icon info-bar__icon\"><use xlink:href=\"#icon-fax1\"></use></svg>
            <a href=\"tel:+48 123 456 789\" class=\"info-bar__link\">+48 (33) 845 73 56</a>
        </li>
        <li class=\"info-bar__item info-bar__item--last\">
            <svg class=\"svg-icon info-bar__icon\"><use xlink:href=\"#icon-email2\"></use></svg>
            <a href=\"mailto:#/\" class=\"info-bar__link\">kontakt@vidigroup.pl</a>
        </li>
    </ul>

    <ul class=\"cell shrink info-bar__list info-bar__list--lang\">
        <li class=\"info-bar__item info-bar__item--lang\">
            <a href=\"/\" class=\"info-bar__item-link\">
                <svg class=\"svg-icon info-bar__icon info-bar__icon--lang\">
                    <use xlink:href=\"#icon-flag-pl\"></use>
                </svg>
            </a>
        </li>
        <li class=\"info-bar__item info-bar__item--lang\">
            <a href=\"/en\" class=\"info-bar__item-link\">
                <svg class=\"svg-icon info-bar__icon info-bar__icon--lang\">
                    <use xlink:href=\"#icon-flag-uk\"></use>
                </svg>
            </a>
        </li>
        <li class=\"info-bar__item info-bar__item--lang\">
            <a href=\"/it\" class=\"info-bar__item-link\">
                <svg class=\"svg-icon info-bar__icon info-bar__icon--lang info-bar__icon--last\">
                    <use xlink:href=\"#icon-flag-it\"></use>
                </svg>
            </a>
        </li>
    </ul>

</div>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/top-nav-info-bar.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Info bar -->
<div class=\"info-bar grid-x show-for-medium\">

    <ul class=\"cell auto info-bar__list\">
        <li class=\"info-bar__title\">Kontakt:</li>
        <li class=\"info-bar__item\">
            <svg class=\"svg-icon info-bar__icon\"><use xlink:href=\"#icon-phone1\"></use></svg>
            <a href=\"tel:+48 123 456 789\" class=\"info-bar__link\">+48 (33) 482 10 67</a>
        </li>
        <li class=\"info-bar__item\">
            <svg class=\"svg-icon info-bar__icon\"><use xlink:href=\"#icon-fax1\"></use></svg>
            <a href=\"tel:+48 123 456 789\" class=\"info-bar__link\">+48 (33) 845 73 56</a>
        </li>
        <li class=\"info-bar__item info-bar__item--last\">
            <svg class=\"svg-icon info-bar__icon\"><use xlink:href=\"#icon-email2\"></use></svg>
            <a href=\"mailto:#/\" class=\"info-bar__link\">kontakt@vidigroup.pl</a>
        </li>
    </ul>

    <ul class=\"cell shrink info-bar__list info-bar__list--lang\">
        <li class=\"info-bar__item info-bar__item--lang\">
            <a href=\"/\" class=\"info-bar__item-link\">
                <svg class=\"svg-icon info-bar__icon info-bar__icon--lang\">
                    <use xlink:href=\"#icon-flag-pl\"></use>
                </svg>
            </a>
        </li>
        <li class=\"info-bar__item info-bar__item--lang\">
            <a href=\"/en\" class=\"info-bar__item-link\">
                <svg class=\"svg-icon info-bar__icon info-bar__icon--lang\">
                    <use xlink:href=\"#icon-flag-uk\"></use>
                </svg>
            </a>
        </li>
        <li class=\"info-bar__item info-bar__item--lang\">
            <a href=\"/it\" class=\"info-bar__item-link\">
                <svg class=\"svg-icon info-bar__icon info-bar__icon--lang info-bar__icon--last\">
                    <use xlink:href=\"#icon-flag-it\"></use>
                </svg>
            </a>
        </li>
    </ul>

</div>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/top-nav-info-bar.htm", "");
    }
}
