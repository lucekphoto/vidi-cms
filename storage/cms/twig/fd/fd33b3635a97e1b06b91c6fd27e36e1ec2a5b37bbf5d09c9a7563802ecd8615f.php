<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/pages/kontakt.htm */
class __TwigTemplate_57e045bcc6132985b53039fc4c0a5ac33fe630628238e8be2ff46a8b49142994 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"grid-container full main-header\">

        <!-- Title bar only on \"small\" screens -->
        ";
        // line 5
        echo "
        <div class=\"top-nav show-for-large\">
    
            <div class=\"grid-container\">
                <div class=\"grid-x grid-margin-x\">
                    <div class=\"cell\">
    
                        <!-- Info bar -->
                        ";
        // line 14
        echo "    
                        <!-- Main navigation -->
                        <div class=\"main-nav show-for-large\">
                            <div class=\"main-nav__logo\">
                                <a href=\"/\" class=\"main-nav__logo-link\">
                                    <img src=\"assets/img/vidi-group-logo-header.png\" srcset=\"assets/img/vidi-group-logo-header.png 1x,
                                                                                 assets/img/vidi-group-logo-header@2x.png 2x,
                                                                                 assets/img/vidi-group-logo-header@3x.png 3x\" class=\"main-nav__logo-img\"
                                        alt=\"Vidi Group logotyp\">
                                </a>
                            </div>
                            <div class=\"main-nav__menu\">
                                <ul class=\"dropdown menu\" data-dropdown-menu>
                                    <li><a href=\"/\">Strona główna</a></li>
                                    <li class=\"is-dropdown-submenu-parent\">
                                        <a href=\"/#\">Usługi</a>
                                        <ul class=\"menu vertical\">
                                          <li><a href=\"/transport-ekspresowy.html\">Transport ekspresowy</a></li>
                                          <li><a href=\"/transport-targi.html\">Transport na targi</a></li>
                                          <li><a href=\"/transport-przesiedlenia.html\">Transport przesiedlenia</a></li>
                                          <li><a href=\"/transport-chlodniczy.html\">Transport towarów chłodniczych</a></li>
                                          <li><a href=\"/transport-adr.html\">Transport towarów niebezpiecznych</a></li>
                                          <li><a href=\"/transport-na-wiszaco.html\">Transport towarów \"na wisząco\"</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/flota.html\">Flota</a></li>
                                    <li class=\"is-dropdown-submenu-parent active\">
                                        <a href=\"/o-firmie.html\">O firmie</a>
                                        <ul class=\"menu vertical\">
                                            <li><a href=\"/zespol.html\">Team</a></li>
                                            <li><a href=\"/dokumenty.html\">Dokumenty</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/#\">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        
        <div class=\"page-header page-header--map\">
            <iframe src=\"https://snazzymaps.com/embed/37565\" width=\"100%\" height=\"500px\" style=\"border:none;\"></iframe>
        </div>
        
    </div>
    
    <section class=\"section-page-content\">
        <div class=\"grid-container\">

            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell small-12\">
                    <div class=\"short-spacer short-spacer--center\"></div>
                    <h2 class=\"section-title\">";
        // line 70
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo "</h2>

                    <div class=\"grid-x grid-margin-x\">
                        <div class=\"cell small-12 medium-4 contact-office\">
                            <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, ipsa! Sint, obcaecati! Aspernatur similique tempora
                            doloremque, quam iusto dicta maxime excepturi, deleniti, nesciunt magni quis culpa velit omnis voluptates veniam?</p> -->
                            <ul class=\"contact-list\">
                                <svg class=\"svg-icon contact-item__icon\"><use xlink:href=\"#icon-pin-line\"></use></svg>
                                <li class=\"contact-list__header\">Nasze biuro:</li>
                                <li class=\"contact-list__item\">VIDI GROUP Sp. Z o. o. Sp. k.</li>
                                <li class=\"contact-list__item\">Ignacego Łukasiewicza 8/1</li>
                                <li class=\"contact-list__item\">43-300 Bielsko-Biała</li>
                                <br/>
                                <li class=\"contact-list__item\"><strong>NIP:</strong> 547-218-49-78</li>
                                <li class=\"contact-list__item\"><strong>REGON:</strong> 000000001</li>
                            </ul>
                        </div>

                        <div class=\"cell small-12 medium-4 contact-numbers\">
                            <ul class=\"contact-list\">
                                <svg class=\"svg-icon contact-item__icon\"><use xlink:href=\"#icon-phone1\"></use></svg>
                                <li class=\"contact-list__header\">Telefon:</li>
                                <li class=\"contact-list__item\"><a href=\"tel:0048666666666\">+48 666 666 666</a></li>
                                <li class=\"contact-list__item\"><a href=\"tel:0048666666666\">+48 666 666 666</a></li>
                                <li class=\"contact-list__item\"><a href=\"tel:0048666666666\">+48 666 666 666</a></li>
                            </ul>
                            <ul class=\"contact-list\">
                                <svg class=\"svg-icon contact-item__icon\"><use xlink:href=\"#icon-fax1\"></use></svg>
                                <li class=\"contact-list__header\">Fax:</li>
                                <li class=\"contact-list__item\"><a href=\"tel:0048666666666\">+48 666 666 666</a></li>
                            </ul>
                            
                        </div>

                        <div class=\"cell small-12 medium-4 contact-numbers\">
                            <ul class=\"contact-list\">
                                <svg class=\"svg-icon contact-item__icon\"><use xlink:href=\"#icon-email2\"></use></svg>
                                <li class=\"contact-list__header\">E-mail:</li>
                                <li class=\"contact-list__item\"><a href=\"mailto:info@vidigroup.pl\">info@vidigroup.pl</a></li>
                            </ul>
                        </div>

                        <div class=\"cell small-12 contact-bank\">
                            <ul class=\"contact-list\">
                                <svg class=\"svg-icon contact-item__icon\"><use xlink:href=\"#icon-bank\"></use></svg>
                                <li class=\"contact-list__header\">Numer konta bankowego</li>
                                <li class=\"contact-list__item\"><strong>PLN:</strong> 7611 4000 0033 3073 6070 2868 04</li>
                                <li class=\"contact-list__item\"><strong>EUR:</strong> 3111 4066 3739 02</li>
                                <li class=\"contact-list__item\"><strong>SWIFT:</strong> BREXPLPWBIB</li>
                            </ul>
                        </div>

                    </div>
    
                </div>
            </div>
            
        </div>
    </section>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/pages/kontakt.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 70,  34 => 14,  24 => 5,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"grid-container full main-header\">

        <!-- Title bar only on \"small\" screens -->
        {# {{> title-bar}} #}

        <div class=\"top-nav show-for-large\">
    
            <div class=\"grid-container\">
                <div class=\"grid-x grid-margin-x\">
                    <div class=\"cell\">
    
                        <!-- Info bar -->
                        {# {{> top-nav-info-bar}} #}
    
                        <!-- Main navigation -->
                        <div class=\"main-nav show-for-large\">
                            <div class=\"main-nav__logo\">
                                <a href=\"/\" class=\"main-nav__logo-link\">
                                    <img src=\"assets/img/vidi-group-logo-header.png\" srcset=\"assets/img/vidi-group-logo-header.png 1x,
                                                                                 assets/img/vidi-group-logo-header@2x.png 2x,
                                                                                 assets/img/vidi-group-logo-header@3x.png 3x\" class=\"main-nav__logo-img\"
                                        alt=\"Vidi Group logotyp\">
                                </a>
                            </div>
                            <div class=\"main-nav__menu\">
                                <ul class=\"dropdown menu\" data-dropdown-menu>
                                    <li><a href=\"/\">Strona główna</a></li>
                                    <li class=\"is-dropdown-submenu-parent\">
                                        <a href=\"/#\">Usługi</a>
                                        <ul class=\"menu vertical\">
                                          <li><a href=\"/transport-ekspresowy.html\">Transport ekspresowy</a></li>
                                          <li><a href=\"/transport-targi.html\">Transport na targi</a></li>
                                          <li><a href=\"/transport-przesiedlenia.html\">Transport przesiedlenia</a></li>
                                          <li><a href=\"/transport-chlodniczy.html\">Transport towarów chłodniczych</a></li>
                                          <li><a href=\"/transport-adr.html\">Transport towarów niebezpiecznych</a></li>
                                          <li><a href=\"/transport-na-wiszaco.html\">Transport towarów \"na wisząco\"</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/flota.html\">Flota</a></li>
                                    <li class=\"is-dropdown-submenu-parent active\">
                                        <a href=\"/o-firmie.html\">O firmie</a>
                                        <ul class=\"menu vertical\">
                                            <li><a href=\"/zespol.html\">Team</a></li>
                                            <li><a href=\"/dokumenty.html\">Dokumenty</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/#\">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        
        <div class=\"page-header page-header--map\">
            <iframe src=\"https://snazzymaps.com/embed/37565\" width=\"100%\" height=\"500px\" style=\"border:none;\"></iframe>
        </div>
        
    </div>
    
    <section class=\"section-page-content\">
        <div class=\"grid-container\">

            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell small-12\">
                    <div class=\"short-spacer short-spacer--center\"></div>
                    <h2 class=\"section-title\">{{ this.page.title }}</h2>

                    <div class=\"grid-x grid-margin-x\">
                        <div class=\"cell small-12 medium-4 contact-office\">
                            <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, ipsa! Sint, obcaecati! Aspernatur similique tempora
                            doloremque, quam iusto dicta maxime excepturi, deleniti, nesciunt magni quis culpa velit omnis voluptates veniam?</p> -->
                            <ul class=\"contact-list\">
                                <svg class=\"svg-icon contact-item__icon\"><use xlink:href=\"#icon-pin-line\"></use></svg>
                                <li class=\"contact-list__header\">Nasze biuro:</li>
                                <li class=\"contact-list__item\">VIDI GROUP Sp. Z o. o. Sp. k.</li>
                                <li class=\"contact-list__item\">Ignacego Łukasiewicza 8/1</li>
                                <li class=\"contact-list__item\">43-300 Bielsko-Biała</li>
                                <br/>
                                <li class=\"contact-list__item\"><strong>NIP:</strong> 547-218-49-78</li>
                                <li class=\"contact-list__item\"><strong>REGON:</strong> 000000001</li>
                            </ul>
                        </div>

                        <div class=\"cell small-12 medium-4 contact-numbers\">
                            <ul class=\"contact-list\">
                                <svg class=\"svg-icon contact-item__icon\"><use xlink:href=\"#icon-phone1\"></use></svg>
                                <li class=\"contact-list__header\">Telefon:</li>
                                <li class=\"contact-list__item\"><a href=\"tel:0048666666666\">+48 666 666 666</a></li>
                                <li class=\"contact-list__item\"><a href=\"tel:0048666666666\">+48 666 666 666</a></li>
                                <li class=\"contact-list__item\"><a href=\"tel:0048666666666\">+48 666 666 666</a></li>
                            </ul>
                            <ul class=\"contact-list\">
                                <svg class=\"svg-icon contact-item__icon\"><use xlink:href=\"#icon-fax1\"></use></svg>
                                <li class=\"contact-list__header\">Fax:</li>
                                <li class=\"contact-list__item\"><a href=\"tel:0048666666666\">+48 666 666 666</a></li>
                            </ul>
                            
                        </div>

                        <div class=\"cell small-12 medium-4 contact-numbers\">
                            <ul class=\"contact-list\">
                                <svg class=\"svg-icon contact-item__icon\"><use xlink:href=\"#icon-email2\"></use></svg>
                                <li class=\"contact-list__header\">E-mail:</li>
                                <li class=\"contact-list__item\"><a href=\"mailto:info@vidigroup.pl\">info@vidigroup.pl</a></li>
                            </ul>
                        </div>

                        <div class=\"cell small-12 contact-bank\">
                            <ul class=\"contact-list\">
                                <svg class=\"svg-icon contact-item__icon\"><use xlink:href=\"#icon-bank\"></use></svg>
                                <li class=\"contact-list__header\">Numer konta bankowego</li>
                                <li class=\"contact-list__item\"><strong>PLN:</strong> 7611 4000 0033 3073 6070 2868 04</li>
                                <li class=\"contact-list__item\"><strong>EUR:</strong> 3111 4066 3739 02</li>
                                <li class=\"contact-list__item\"><strong>SWIFT:</strong> BREXPLPWBIB</li>
                            </ul>
                        </div>

                    </div>
    
                </div>
            </div>
            
        </div>
    </section>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/pages/kontakt.htm", "");
    }
}
