<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/partials/off-canvas-contact.htm */
class __TwigTemplate_cdc4177bcf844ec7896845a0ddc07454da272f4cf2729b879e8099a2bf44aded extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"off-canvas off-canvas-contact hide-for-large position-right\" id=\"offCanvasContact\" data-off-canvas data-transition=\"overlap\">

    <!-- OffCanvas panel close button -->
    <button class=\"close-button\" aria-label=\"Close menu\" type=\"button\" data-close>
        <span aria-hidden=\"true\">&times;</span>
    </button>

    <!-- OffCanvas panel content lives here -->
    <h3 class=\"off-canvas-content__header\">Kontakt</h3>
    <ul class=\"off-canvas-content-list\">
        <li class=\"off-canvas-content__item\">
            VIDI GROUP Sp. Z o. o. Sp. k.
            <br />Ignacego Łukasiewicza 8/1
            <br />43-300, Bielsko-Biała
            <br />
            <br />NIP: 547-218-49-78
            <br />REGON: 000000001
        </li>
        <li class=\"off-canvas-content__item\">
            <strong>Masz pytania?</strong>
            <a href=\"mailto:kontakt@vidigroup.pl\">kontakt@vidigroup.pl</a>
        </li>
        <li class=\"off-canvas-content__item\">
            <strong>Telefon/Fax:</strong>
            <a href=\"tel:+48 33 482 10 67\">+48 (33) 482 10 67</a>
            <a href=\"tel:+48 33 845 73 56\">+48 (33) 845 73 56</a>
        </li>
    </ul>

</div>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/off-canvas-contact.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"off-canvas off-canvas-contact hide-for-large position-right\" id=\"offCanvasContact\" data-off-canvas data-transition=\"overlap\">

    <!-- OffCanvas panel close button -->
    <button class=\"close-button\" aria-label=\"Close menu\" type=\"button\" data-close>
        <span aria-hidden=\"true\">&times;</span>
    </button>

    <!-- OffCanvas panel content lives here -->
    <h3 class=\"off-canvas-content__header\">Kontakt</h3>
    <ul class=\"off-canvas-content-list\">
        <li class=\"off-canvas-content__item\">
            VIDI GROUP Sp. Z o. o. Sp. k.
            <br />Ignacego Łukasiewicza 8/1
            <br />43-300, Bielsko-Biała
            <br />
            <br />NIP: 547-218-49-78
            <br />REGON: 000000001
        </li>
        <li class=\"off-canvas-content__item\">
            <strong>Masz pytania?</strong>
            <a href=\"mailto:kontakt@vidigroup.pl\">kontakt@vidigroup.pl</a>
        </li>
        <li class=\"off-canvas-content__item\">
            <strong>Telefon/Fax:</strong>
            <a href=\"tel:+48 33 482 10 67\">+48 (33) 482 10 67</a>
            <a href=\"tel:+48 33 845 73 56\">+48 (33) 845 73 56</a>
        </li>
    </ul>

</div>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/off-canvas-contact.htm", "");
    }
}
