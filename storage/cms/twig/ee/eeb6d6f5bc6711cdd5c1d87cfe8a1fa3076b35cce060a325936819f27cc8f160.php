<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/partials/off-canvas-menu.htm */
class __TwigTemplate_ea0f35601e7b083c1367f5cf52711bba98e1c90f243b0cff83be1141a340c39f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"off-canvas hide-for-large position-left\" id=\"offCanvasMenu\" data-off-canvas data-transition=\"overlap\">

    <!-- OffCanvas panel close button -->
    <button class=\"close-button\" aria-label=\"Close menu\" type=\"button\" data-close>
        <span aria-hidden=\"true\">&times;</span>
    </button>

    <!-- OffCanvas panel content lives here -->
    <ul class=\"vertical menu accordion-menu\" data-accordion-menu>
        <li><a href=\"/\">Strona główna</a></li>
        <li>
            <a href=\"#/\">Usługi</a>
            <ul class=\"menu vertical nested\">
                <li><a href=\"/transport-ekspresowy.html\">Transport ekspresowy</a></li>
                <li><a href=\"/transport-targi.html\">Transport na targi</a></li>
                <li><a href=\"/transport-przesiedlenia.html\">Transport przesiedlenia</a></li>
                <li><a href=\"/transport-chlodniczy.html\">Transport towarów chłodniczych</a></li>
                <li><a href=\"/transport-adr.html\">Transport towarów niebezpiecznych</a></li>
                <li><a href=\"/transport-na-wiszaco.html\">Transport towarów \"na wisząco\"</a></li>
            </ul>
        </li>
        <li><a href=\"/flota.html\">Flota</a></li>
        <li><a href=\"/o-firmie.html\">O firmie</a></li>
        <li><a href=\"/zespol.html\">Zespół</a></li>
        <li><a href=\"/kontakt.html\">Kontakt</a></li>
    </ul>

</div>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/off-canvas-menu.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"off-canvas hide-for-large position-left\" id=\"offCanvasMenu\" data-off-canvas data-transition=\"overlap\">

    <!-- OffCanvas panel close button -->
    <button class=\"close-button\" aria-label=\"Close menu\" type=\"button\" data-close>
        <span aria-hidden=\"true\">&times;</span>
    </button>

    <!-- OffCanvas panel content lives here -->
    <ul class=\"vertical menu accordion-menu\" data-accordion-menu>
        <li><a href=\"/\">Strona główna</a></li>
        <li>
            <a href=\"#/\">Usługi</a>
            <ul class=\"menu vertical nested\">
                <li><a href=\"/transport-ekspresowy.html\">Transport ekspresowy</a></li>
                <li><a href=\"/transport-targi.html\">Transport na targi</a></li>
                <li><a href=\"/transport-przesiedlenia.html\">Transport przesiedlenia</a></li>
                <li><a href=\"/transport-chlodniczy.html\">Transport towarów chłodniczych</a></li>
                <li><a href=\"/transport-adr.html\">Transport towarów niebezpiecznych</a></li>
                <li><a href=\"/transport-na-wiszaco.html\">Transport towarów \"na wisząco\"</a></li>
            </ul>
        </li>
        <li><a href=\"/flota.html\">Flota</a></li>
        <li><a href=\"/o-firmie.html\">O firmie</a></li>
        <li><a href=\"/zespol.html\">Zespół</a></li>
        <li><a href=\"/kontakt.html\">Kontakt</a></li>
    </ul>

</div>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/off-canvas-menu.htm", "");
    }
}
