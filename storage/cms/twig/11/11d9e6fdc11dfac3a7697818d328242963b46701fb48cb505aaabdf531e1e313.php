<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/partials/contact-callout.htm */
class __TwigTemplate_76e04965bd723852cef2bde671def94614af7da71b6eb8a84b2efa54f86bab9c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"section-btm-callout section-full section-full--secondary\" style=\"background-image: url(";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/hero-contact.jpg");
        echo ");\">
    <div class=\"grid-container section-btm-callout-inner\">

        <div class=\"grid-x grid-margin-x align-middle align-center\">
            <div class=\"cell small-12 medium-8 large-7 section-btm-callout-lead\">
                <h2 class=\"\">Nie znalazłeś w naszej ofercie odpowiedniej usługi?</h2>
                <p>Nic nie szkodzi! Skontaktuj się z nami. Nasz doświadczony zespół specjalistów poradzi sobie z każdym zadaniem</p>
            </div>
            <div class=\"cell small-12 medium-4 large-3 section-btm-callout-contacts\">
                <a href=\"";
        // line 10
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("kontakt");
        echo "\" class=\"button expanded primary button--nomargin\">Napisz do nas</a>
                <div class=\"section-btm-callout__separator-wrapper\">
                    <span class=\"section-btm-callout__separator\">lub zadzwoń</span>
                </div>
                <a href=\"tel:000000000\" class=\"section-btm-callout__link\">+48 123 456 789</a>
            </div>
        </div>

    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/contact-callout.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 10,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"section-btm-callout section-full section-full--secondary\" style=\"background-image: url({{ 'assets/img/hero-contact.jpg'|theme }});\">
    <div class=\"grid-container section-btm-callout-inner\">

        <div class=\"grid-x grid-margin-x align-middle align-center\">
            <div class=\"cell small-12 medium-8 large-7 section-btm-callout-lead\">
                <h2 class=\"\">Nie znalazłeś w naszej ofercie odpowiedniej usługi?</h2>
                <p>Nic nie szkodzi! Skontaktuj się z nami. Nasz doświadczony zespół specjalistów poradzi sobie z każdym zadaniem</p>
            </div>
            <div class=\"cell small-12 medium-4 large-3 section-btm-callout-contacts\">
                <a href=\"{{ 'kontakt'|page }}\" class=\"button expanded primary button--nomargin\">Napisz do nas</a>
                <div class=\"section-btm-callout__separator-wrapper\">
                    <span class=\"section-btm-callout__separator\">lub zadzwoń</span>
                </div>
                <a href=\"tel:000000000\" class=\"section-btm-callout__link\">+48 123 456 789</a>
            </div>
        </div>

    </div>
</section>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/contact-callout.htm", "");
    }
}
