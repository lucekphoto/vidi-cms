<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/partials/off-canvas-language.htm */
class __TwigTemplate_ab4a8da6c9b86981611f1710a656abb7f74e4285dcfa1e64554e7db243f65d9b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"off-canvas off-canvas-lang hide-for-large position-top\" id=\"offCanvasLang\" data-off-canvas data-transition=\"overlap\">

    <!-- OffCanvas panel close button -->
    <button class=\"close-button\" aria-label=\"Close menu\" type=\"button\" data-close>
        <span aria-hidden=\"true\">&times;</span>
    </button>

    <!-- OffCanvas panel content lives here -->
    <div class=\"grid-container full\" style=\"height:100%\">
        <div class=\"grid-x grid-margin-x align-center align-middle\" style=\"height:100%\">
            <div class=\"column off-canvas-lang-wrapper\">
                <a href=\"/\" class=\"off-canvas-lang-link\">
                    <svg class=\"svg-icon off-canvas-lang-icon\">
                        <use xlink:href=\"#icon-flag-pl\"></use>
                    </svg>
                </a>
                <a href=\"/en\" class=\"off-canvas-lang-link\">
                    <svg class=\"svg-icon off-canvas-lang-icon\">
                        <use xlink:href=\"#icon-flag-uk\"></use>
                    </svg>
                </a>
                <a href=\"/it\" class=\"off-canvas-lang-link\">
                    <svg class=\"svg-icon off-canvas-lang-icon\">
                        <use xlink:href=\"#icon-flag-it\"></use>
                    </svg>
                </a>
            </div>
        </div>
    </div>

</div>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/off-canvas-language.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"off-canvas off-canvas-lang hide-for-large position-top\" id=\"offCanvasLang\" data-off-canvas data-transition=\"overlap\">

    <!-- OffCanvas panel close button -->
    <button class=\"close-button\" aria-label=\"Close menu\" type=\"button\" data-close>
        <span aria-hidden=\"true\">&times;</span>
    </button>

    <!-- OffCanvas panel content lives here -->
    <div class=\"grid-container full\" style=\"height:100%\">
        <div class=\"grid-x grid-margin-x align-center align-middle\" style=\"height:100%\">
            <div class=\"column off-canvas-lang-wrapper\">
                <a href=\"/\" class=\"off-canvas-lang-link\">
                    <svg class=\"svg-icon off-canvas-lang-icon\">
                        <use xlink:href=\"#icon-flag-pl\"></use>
                    </svg>
                </a>
                <a href=\"/en\" class=\"off-canvas-lang-link\">
                    <svg class=\"svg-icon off-canvas-lang-icon\">
                        <use xlink:href=\"#icon-flag-uk\"></use>
                    </svg>
                </a>
                <a href=\"/it\" class=\"off-canvas-lang-link\">
                    <svg class=\"svg-icon off-canvas-lang-icon\">
                        <use xlink:href=\"#icon-flag-it\"></use>
                    </svg>
                </a>
            </div>
        </div>
    </div>

</div>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/off-canvas-language.htm", "");
    }
}
