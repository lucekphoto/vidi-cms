<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/pages/transport-adr.htm */
class __TwigTemplate_bf4965bdd83127f135190fca97e19b6eb87763e280dc8bed06eb93c52b562a97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"grid-container full main-header\">

        <!-- Title bar only on \"small\" screens -->
        ";
        // line 5
        echo "
        <div class=\"top-nav show-for-large\">
    
            <div class=\"grid-container\">
                <div class=\"grid-x grid-margin-x\">
                    <div class=\"cell\">
    
                        <!-- Info bar -->
                        ";
        // line 14
        echo "    
                        <!-- Main navigation -->
                        <div class=\"main-nav show-for-large\">
                            <div class=\"main-nav__logo\">
                                <a href=\"/\" class=\"main-nav__logo-link\">
                                    <img src=\"assets/img/vidi-group-logo-header.png\" srcset=\"assets/img/vidi-group-logo-header.png 1x,
                                                                                 assets/img/vidi-group-logo-header@2x.png 2x,
                                                                                 assets/img/vidi-group-logo-header@3x.png 3x\" class=\"main-nav__logo-img\"
                                        alt=\"Vidi Group logotyp\">
                                </a>
                            </div>
                            <div class=\"main-nav__menu\">
                                <ul class=\"dropdown menu\" data-dropdown-menu>
                                    <li><a href=\"/\">Strona główna</a></li>
                                    <li class=\"is-dropdown-submenu-parent\">
                                        <a href=\"/#\">Usługi</a>
                                        <ul class=\"menu vertical\">
                                          <li><a href=\"/transport-ekspresowy.html\">Transport ekspresowy</a></li>
                                          <li><a href=\"/transport-targi.html\">Transport na targi</a></li>
                                          <li><a href=\"/transport-przesiedlenia.html\">Transport przesiedlenia</a></li>
                                          <li><a href=\"/transport-chlodniczy.html\">Transport towarów chłodniczych</a></li>
                                          <li><a href=\"/transport-adr.html\">Transport towarów niebezpiecznych</a></li>
                                          <li><a href=\"/transport-na-wiszaco.html\">Transport towarów \"na wisząco\"</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/flota.html\">Flota</a></li>
                                    <li class=\"is-dropdown-submenu-parent\">
                                        <a href=\"/o-firmie.html\">O firmie</a>
                                        <ul class=\"menu vertical\">
                                            <li><a href=\"/zespol.html\">Team</a></li>
                                            <li><a href=\"/dokumenty.html\">Dokumenty</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/kontakt.html\">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        
        <div class=\"page-header\" style=\"background-image: url(assets/img/page-header/header-17.jpg)\">
            <div class=\"grid-container page-header-inner\">
                <div class=\"page-header-inner-content\">
                    <h1>Transport substancji niebezpiecznych</h1>
                    <nav aria-label=\"Jesteś tutaj:\" role=\"navigation\" class=\"breadcrumbs-wrapper\">
                        <ul class=\"breadcrumbs\">
                            <li><a href=\"/\">Strona główna</a></li>
                            <li><a href=\"/oferta.html\">Oferta</a></li>
                            <li>Transport ADR</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
    </div>
    
    <section class=\"section-page-content\">
        <div class=\"grid-container\">

            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell small-12\">
                    <div class=\"short-spacer short-spacer--center\"></div>
                    <h2 class=\"section-title\">Z nami jesteś bezpieczny</h2>
                    
                    <div class=\"grid-x grid-margin-x service-description\">
                        <div class=\"cell small-12 medium-6\">
                            <p>Potrzebujesz przewieźć towar niebezpieczny? Chętnie pomożemy. Podeślij nam niezbędne do wyceny dane jak: numer rozpoznawczy
                            materiału (UN Number, Numer Rozpoznawczy Materiału), klasę towaru niebezpiecznego (CL, Class, Klasa) oraz grupę w jakiej
                            jest on pakowany (PG, Packing Group, Grupa Pakowania). Na podstawie tych danych ocenimy czy dany towar jest ADR&#39;em na
                            wyłączeniu i wymaga tylko gaśnicy przeciwpożarowej 2 kg na pokładzie, czy też musimy użyć licencjonowanego rozwiązania ADR.</p>
                            <p>Wykorzystywane pojazdy do transportu tkz. pełnych ADR są wyposażone w komplet ADR: środki ochrony indywidualnej, przeciwpożarowej
                            oraz są dostosowane do specjalistycznego oznakowania - a ich kierowcy są w odpowiedni sposób przeszkoleni.</p>                             
                        </div>
                        <div class=\"cell small-12 medium-6\">
                            <img src=\"";
        // line 93
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/page-service/service-adr.jpg");
        echo "\" alt=\"\">
                        </div>
                    </div>

                    <div class=\"grid-x grid-margin-x service-bullets\">
                        <div class=\"cell small-12\">
                            <div class=\"short-spacer\"></div>
                            <h3>Przewozimy materiały ADR klasy: 2, 3, 4, 5, 6, 8 i 9:</h3>
                            <ul class=\"adr-sign-list\">
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Klasa_2.1_ADR.svg/120px-Klasa_2.1_ADR.svg.png\" alt=\"Gazy palne\" title=\"Gazy palne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Klasa_2.2_ADR.svg/120px-Klasa_2.2_ADR.svg.png\" alt=\"Gazy niepalne\" title=\"Gazy niepalne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Klasa_2.3_ADR.svg/120px-Klasa_2.3_ADR.svg.png\" alt=\"Gazy trujące\"
                                        title=\"Gazy trujące\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Klasa_3_ADR.svg/120px-Klasa_3_ADR.svg.png\" alt=\"Ciecze zapalne\"
                                        title=\"Ciecze zapalne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Klasa_4.1_ADR.svg/120px-Klasa_4.1_ADR.svg.png\" alt=\"Materiały stałe zapalne\"
                                        title=\"Materiały stałe zapalne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/Klasa_4.2_ADR.svg/120px-Klasa_4.2_ADR.svg.png\" alt=\"Materiały samozapalne\"
                                        title=\"Materiały samozapalne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Klasa_4.3_ADR.svg/120px-Klasa_4.3_ADR.svg.png\" alt=\"Niebezpieczne w zetknięciu z wodą\"
                                        title=\"Niebezpieczne w zetknięciu z wodą\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Klasa_5.1_ADR.svg/120px-Klasa_5.1_ADR.svg.png\" alt=\"Utleniające\"
                                        title=\"Utleniające\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\"
                                        tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Klasa_5.2_ADR.svg/120px-Klasa_5.2_ADR.svg.png\" alt=\"Nadtlenki organiczne\"
                                        title=\"Nadtlenki organiczne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Klasa_6.1_ADR.svg/120px-Klasa_6.1_ADR.svg.png\" alt=\"Trujące\"
                                        title=\"Trujące\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Dangclass6_2.svg/120px-Dangclass6_2.svg.png\" alt=\"Zakaźne\"
                                        title=\"Zakaźne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Klasa_8_ADR.svg/120px-Klasa_8_ADR.svg.png\" alt=\"Żrące\"
                                        title=\"Żrące\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Dangclass9.svg/120px-Dangclass9.svg.png\" alt=\"Niebezpieczne\"
                                        title=\"Niebezpieczne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class=\"grid-x grid-margin-x service-bullets\">
                        <div class=\"cell small-12 medium-6\">
                            <div class=\"short-spacer\"></div>
                            <h3>Dostarczamy:</h3>
                            <ul class=\"icon-list icon-list--checkbox\">
                                <li class=\"icon-list__item\">
                                    <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                    dedykowany samochód wraz z odpowiednim wyposażeniem do przewozu materiałów niebezpiecznych
                                </li>
                                <li class=\"icon-list__item\">
                                    <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                    wyszkolonego kierowcę
                                </li>
                                <li class=\"icon-list__item\">
                                    <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                     transport w systemie door to door, JIT, bez przeładunku
                                </li>
                                <li class=\"icon-list__item\">
                                    <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                    kontakt ze spedytorem 24/7
                                </li>
                                <li class=\"icon-list__item\">
                                    <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                    GPS
                                </li>
                            </ul>
                        </div>
                        <div class=\"cell small-12 medium-6\">
                            <div class=\"short-spacer\"></div>
                            <h3>Zasieg: EU + NO + CH</h3>
                            <img src=\"";
        // line 187
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/service-map.jpg");
        echo "\" srcset=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/service-map.jpg");
        echo " 1x, ";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/service-map@2x.jpg");
        echo " 2x, ";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/service-map@3x.jpg");
        echo " 3x\" alt=\"Mapa Europy zasięg działalności\">
                        </div>
                    </div>

                    <div class=\"grid-x grid-margin-x service-fleet\">
                        <div class=\"cell small-12\">
                            <div class=\"short-spacer\"></div>
                            <h3>Zalecany środek transportu:</h3>
                            <div class=\"grid-x grid-margin-x\">
                    
                                <div class=\"cell small-6 large-3 service-fleet__item\">
                                    <a href=\"";
        // line 198
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("flota");
        echo "#panel1\" class=\"service-fleet__link\">
                                        <div class=\"service-fleet__img\">
                                            <img src=\"";
        // line 200
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/small/bus_5ep_nosize.png");
        echo "\" alt=\"\">
                                        </div>
                                        <div class=\"service-fleet__title\">Bus - 5 EP</div>
                                    </a>
                                </div>
                    
                                <div class=\"cell small-6 large-3 service-fleet__item\">
                                    <a href=\"";
        // line 207
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("flota");
        echo "#panel3\" class=\"service-fleet__link\">
                                        <div class=\"service-fleet__img\">
                                            <img src=\"";
        // line 209
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/small/bus_8ep_plandeka_nosize.png");
        echo "\" alt=\"\">
                                        </div>
                                        <div class=\"service-fleet__title\">Bus - 8 EP (plandeka)</div>
                                    </a>
                                </div>
                    
                            
                                <div class=\"cell small-6 large-3 service-fleet__item\">
                                    <a href=\"";
        // line 217
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("flota");
        echo "#panel7\" class=\"service-fleet__link\">
                                        <div class=\"service-fleet__img\">
                                            <img src=\"";
        // line 219
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/small/bus_8ep_plandeka_lift_trolley_nosize.png");
        echo "\" alt=\"\">
                                        </div>
                                        <div class=\"service-fleet__title\">Bus - 8 EP (plandeka, winda)</div>
                                    </a>
                                </div>

                                <div class=\"cell small-6 large-3 service-fleet__item\">
                                    <a href=\"";
        // line 226
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("flota");
        echo "#panel5\" class=\"service-fleet__link\">
                                        <div class=\"service-fleet__img\">
                                            <img src=\"";
        // line 228
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/fleet/small/bus_8ep_fridge_nosize.png");
        echo "\" alt=\"\">
                                        </div>
                                        <div class=\"service-fleet__title\">Bus - 8 EP (chłodnia)</div>
                                    </a>
                                </div>
                    
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </section>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/pages/transport-adr.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 228,  278 => 226,  268 => 219,  263 => 217,  252 => 209,  247 => 207,  237 => 200,  232 => 198,  212 => 187,  115 => 93,  34 => 14,  24 => 5,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"grid-container full main-header\">

        <!-- Title bar only on \"small\" screens -->
        {# {{> title-bar}} #}

        <div class=\"top-nav show-for-large\">
    
            <div class=\"grid-container\">
                <div class=\"grid-x grid-margin-x\">
                    <div class=\"cell\">
    
                        <!-- Info bar -->
                        {# {{> top-nav-info-bar}} #}
    
                        <!-- Main navigation -->
                        <div class=\"main-nav show-for-large\">
                            <div class=\"main-nav__logo\">
                                <a href=\"/\" class=\"main-nav__logo-link\">
                                    <img src=\"assets/img/vidi-group-logo-header.png\" srcset=\"assets/img/vidi-group-logo-header.png 1x,
                                                                                 assets/img/vidi-group-logo-header@2x.png 2x,
                                                                                 assets/img/vidi-group-logo-header@3x.png 3x\" class=\"main-nav__logo-img\"
                                        alt=\"Vidi Group logotyp\">
                                </a>
                            </div>
                            <div class=\"main-nav__menu\">
                                <ul class=\"dropdown menu\" data-dropdown-menu>
                                    <li><a href=\"/\">Strona główna</a></li>
                                    <li class=\"is-dropdown-submenu-parent\">
                                        <a href=\"/#\">Usługi</a>
                                        <ul class=\"menu vertical\">
                                          <li><a href=\"/transport-ekspresowy.html\">Transport ekspresowy</a></li>
                                          <li><a href=\"/transport-targi.html\">Transport na targi</a></li>
                                          <li><a href=\"/transport-przesiedlenia.html\">Transport przesiedlenia</a></li>
                                          <li><a href=\"/transport-chlodniczy.html\">Transport towarów chłodniczych</a></li>
                                          <li><a href=\"/transport-adr.html\">Transport towarów niebezpiecznych</a></li>
                                          <li><a href=\"/transport-na-wiszaco.html\">Transport towarów \"na wisząco\"</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/flota.html\">Flota</a></li>
                                    <li class=\"is-dropdown-submenu-parent\">
                                        <a href=\"/o-firmie.html\">O firmie</a>
                                        <ul class=\"menu vertical\">
                                            <li><a href=\"/zespol.html\">Team</a></li>
                                            <li><a href=\"/dokumenty.html\">Dokumenty</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"/kontakt.html\">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        
        <div class=\"page-header\" style=\"background-image: url(assets/img/page-header/header-17.jpg)\">
            <div class=\"grid-container page-header-inner\">
                <div class=\"page-header-inner-content\">
                    <h1>Transport substancji niebezpiecznych</h1>
                    <nav aria-label=\"Jesteś tutaj:\" role=\"navigation\" class=\"breadcrumbs-wrapper\">
                        <ul class=\"breadcrumbs\">
                            <li><a href=\"/\">Strona główna</a></li>
                            <li><a href=\"/oferta.html\">Oferta</a></li>
                            <li>Transport ADR</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
    </div>
    
    <section class=\"section-page-content\">
        <div class=\"grid-container\">

            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell small-12\">
                    <div class=\"short-spacer short-spacer--center\"></div>
                    <h2 class=\"section-title\">Z nami jesteś bezpieczny</h2>
                    
                    <div class=\"grid-x grid-margin-x service-description\">
                        <div class=\"cell small-12 medium-6\">
                            <p>Potrzebujesz przewieźć towar niebezpieczny? Chętnie pomożemy. Podeślij nam niezbędne do wyceny dane jak: numer rozpoznawczy
                            materiału (UN Number, Numer Rozpoznawczy Materiału), klasę towaru niebezpiecznego (CL, Class, Klasa) oraz grupę w jakiej
                            jest on pakowany (PG, Packing Group, Grupa Pakowania). Na podstawie tych danych ocenimy czy dany towar jest ADR&#39;em na
                            wyłączeniu i wymaga tylko gaśnicy przeciwpożarowej 2 kg na pokładzie, czy też musimy użyć licencjonowanego rozwiązania ADR.</p>
                            <p>Wykorzystywane pojazdy do transportu tkz. pełnych ADR są wyposażone w komplet ADR: środki ochrony indywidualnej, przeciwpożarowej
                            oraz są dostosowane do specjalistycznego oznakowania - a ich kierowcy są w odpowiedni sposób przeszkoleni.</p>                             
                        </div>
                        <div class=\"cell small-12 medium-6\">
                            <img src=\"{{ 'assets/img/page-service/service-adr.jpg'|theme }}\" alt=\"\">
                        </div>
                    </div>

                    <div class=\"grid-x grid-margin-x service-bullets\">
                        <div class=\"cell small-12\">
                            <div class=\"short-spacer\"></div>
                            <h3>Przewozimy materiały ADR klasy: 2, 3, 4, 5, 6, 8 i 9:</h3>
                            <ul class=\"adr-sign-list\">
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Klasa_2.1_ADR.svg/120px-Klasa_2.1_ADR.svg.png\" alt=\"Gazy palne\" title=\"Gazy palne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Klasa_2.2_ADR.svg/120px-Klasa_2.2_ADR.svg.png\" alt=\"Gazy niepalne\" title=\"Gazy niepalne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Klasa_2.3_ADR.svg/120px-Klasa_2.3_ADR.svg.png\" alt=\"Gazy trujące\"
                                        title=\"Gazy trujące\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Klasa_3_ADR.svg/120px-Klasa_3_ADR.svg.png\" alt=\"Ciecze zapalne\"
                                        title=\"Ciecze zapalne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Klasa_4.1_ADR.svg/120px-Klasa_4.1_ADR.svg.png\" alt=\"Materiały stałe zapalne\"
                                        title=\"Materiały stałe zapalne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/Klasa_4.2_ADR.svg/120px-Klasa_4.2_ADR.svg.png\" alt=\"Materiały samozapalne\"
                                        title=\"Materiały samozapalne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Klasa_4.3_ADR.svg/120px-Klasa_4.3_ADR.svg.png\" alt=\"Niebezpieczne w zetknięciu z wodą\"
                                        title=\"Niebezpieczne w zetknięciu z wodą\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Klasa_5.1_ADR.svg/120px-Klasa_5.1_ADR.svg.png\" alt=\"Utleniające\"
                                        title=\"Utleniające\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\"
                                        tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Klasa_5.2_ADR.svg/120px-Klasa_5.2_ADR.svg.png\" alt=\"Nadtlenki organiczne\"
                                        title=\"Nadtlenki organiczne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Klasa_6.1_ADR.svg/120px-Klasa_6.1_ADR.svg.png\" alt=\"Trujące\"
                                        title=\"Trujące\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Dangclass6_2.svg/120px-Dangclass6_2.svg.png\" alt=\"Zakaźne\"
                                        title=\"Zakaźne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Klasa_8_ADR.svg/120px-Klasa_8_ADR.svg.png\" alt=\"Żrące\"
                                        title=\"Żrące\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                                <li class=\"adr-sign-item\">
                                    <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Dangclass9.svg/120px-Dangclass9.svg.png\" alt=\"Niebezpieczne\"
                                        title=\"Niebezpieczne\" data-tooltip aria-haspopup=\"true\" class=\"has-tip top\" data-disable-hover=\"false\" tabindex=\"2\">
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class=\"grid-x grid-margin-x service-bullets\">
                        <div class=\"cell small-12 medium-6\">
                            <div class=\"short-spacer\"></div>
                            <h3>Dostarczamy:</h3>
                            <ul class=\"icon-list icon-list--checkbox\">
                                <li class=\"icon-list__item\">
                                    <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                    dedykowany samochód wraz z odpowiednim wyposażeniem do przewozu materiałów niebezpiecznych
                                </li>
                                <li class=\"icon-list__item\">
                                    <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                    wyszkolonego kierowcę
                                </li>
                                <li class=\"icon-list__item\">
                                    <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                     transport w systemie door to door, JIT, bez przeładunku
                                </li>
                                <li class=\"icon-list__item\">
                                    <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                    kontakt ze spedytorem 24/7
                                </li>
                                <li class=\"icon-list__item\">
                                    <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                    GPS
                                </li>
                            </ul>
                        </div>
                        <div class=\"cell small-12 medium-6\">
                            <div class=\"short-spacer\"></div>
                            <h3>Zasieg: EU + NO + CH</h3>
                            <img src=\"{{'assets/img/service-map.jpg'|theme }}\" srcset=\"{{'assets/img/service-map.jpg'|theme }} 1x, {{'assets/img/service-map@2x.jpg'|theme }} 2x, {{'assets/img/service-map@3x.jpg'|theme }} 3x\" alt=\"Mapa Europy zasięg działalności\">
                        </div>
                    </div>

                    <div class=\"grid-x grid-margin-x service-fleet\">
                        <div class=\"cell small-12\">
                            <div class=\"short-spacer\"></div>
                            <h3>Zalecany środek transportu:</h3>
                            <div class=\"grid-x grid-margin-x\">
                    
                                <div class=\"cell small-6 large-3 service-fleet__item\">
                                    <a href=\"{{ 'flota'|page }}#panel1\" class=\"service-fleet__link\">
                                        <div class=\"service-fleet__img\">
                                            <img src=\"{{'assets/img/fleet/small/bus_5ep_nosize.png'|theme }}\" alt=\"\">
                                        </div>
                                        <div class=\"service-fleet__title\">Bus - 5 EP</div>
                                    </a>
                                </div>
                    
                                <div class=\"cell small-6 large-3 service-fleet__item\">
                                    <a href=\"{{ 'flota'|page }}#panel3\" class=\"service-fleet__link\">
                                        <div class=\"service-fleet__img\">
                                            <img src=\"{{'assets/img/fleet/small/bus_8ep_plandeka_nosize.png'|theme }}\" alt=\"\">
                                        </div>
                                        <div class=\"service-fleet__title\">Bus - 8 EP (plandeka)</div>
                                    </a>
                                </div>
                    
                            
                                <div class=\"cell small-6 large-3 service-fleet__item\">
                                    <a href=\"{{ 'flota'|page }}#panel7\" class=\"service-fleet__link\">
                                        <div class=\"service-fleet__img\">
                                            <img src=\"{{'assets/img/fleet/small/bus_8ep_plandeka_lift_trolley_nosize.png'|theme }}\" alt=\"\">
                                        </div>
                                        <div class=\"service-fleet__title\">Bus - 8 EP (plandeka, winda)</div>
                                    </a>
                                </div>

                                <div class=\"cell small-6 large-3 service-fleet__item\">
                                    <a href=\"{{ 'flota'|page }}#panel5\" class=\"service-fleet__link\">
                                        <div class=\"service-fleet__img\">
                                            <img src=\"{{'assets/img/fleet/small/bus_8ep_fridge_nosize.png'|theme }}\" alt=\"\">
                                        </div>
                                        <div class=\"service-fleet__title\">Bus - 8 EP (chłodnia)</div>
                                    </a>
                                </div>
                    
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </section>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/pages/transport-adr.htm", "");
    }
}
