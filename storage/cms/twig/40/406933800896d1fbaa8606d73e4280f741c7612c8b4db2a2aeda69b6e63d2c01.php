<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/layouts/default.htm */
class __TwigTemplate_3d1de705f4432629bdcebbb7d8f25190b8e03828c3a60545be992b902c1eaf98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"no-js\" lang=\"pl\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <title>October CMS - ";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo "</title>
        <meta name=\"description\" content=\"";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "meta_description", array()), "html", null, true);
        echo "\">
        <meta name=\"title\" content=\"";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "meta_title", array()), "html", null, true);
        echo "\">
        <meta name=\"author\" content=\"OctoberCMS\">
        <meta name=\"generator\" content=\"OctoberCMS\">
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 12
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/october.png");
        echo "\">
        <!-- Zamienic na google web font loader -->
        <link href=\"https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i&amp;subset=latin-ext\" rel=\"stylesheet\">
        <link href=\"";
        // line 15
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/app.css");
        echo "\" rel=\"stylesheet\">
    </head>

    <body>

        <!-- Header -->
    
        <!-- Svg icon sprite -->
        ";
        // line 23
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("global/icons"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 24
        echo "        <div class=\"off-canvas-wrapper\">

            <!-- Off-canvas panels -->
            ";
        // line 27
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("off-canvas-menu"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 28
        echo "            ";
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("off-canvas-contact"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 29
        echo "            ";
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("off-canvas-language"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 30
        echo "            
            <div class=\"off-canvas-content\" data-off-canvas-content>

                <!-- Page content -->
                ";
        // line 34
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 35
        echo "                <!-- Contact callout -->
                ";
        // line 36
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("contact-callout"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 37
        echo "                <!-- Footer -->
                ";
        // line 38
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("global/footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 39
        echo "
            </div>

        </div>

        <!-- Scripts -->
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.0/js/swiper.min.js\"></script>
        <script src=\"";
        // line 46
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/js/app.js");
        echo "\"></script>

    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/layouts/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 46,  103 => 39,  99 => 38,  96 => 37,  92 => 36,  89 => 35,  87 => 34,  81 => 30,  76 => 29,  71 => 28,  67 => 27,  62 => 24,  58 => 23,  47 => 15,  41 => 12,  35 => 9,  31 => 8,  27 => 7,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html class=\"no-js\" lang=\"pl\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <title>October CMS - {{ this.page.title }}</title>
        <meta name=\"description\" content=\"{{ this.page.meta_description }}\">
        <meta name=\"title\" content=\"{{ this.page.meta_title }}\">
        <meta name=\"author\" content=\"OctoberCMS\">
        <meta name=\"generator\" content=\"OctoberCMS\">
        <link rel=\"icon\" type=\"image/png\" href=\"{{ 'assets/images/october.png'|theme }}\">
        <!-- Zamienic na google web font loader -->
        <link href=\"https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i&amp;subset=latin-ext\" rel=\"stylesheet\">
        <link href=\"{{ 'assets/css/app.css'|theme }}\" rel=\"stylesheet\">
    </head>

    <body>

        <!-- Header -->
    
        <!-- Svg icon sprite -->
        {% partial 'global/icons' %}
        <div class=\"off-canvas-wrapper\">

            <!-- Off-canvas panels -->
            {% partial 'off-canvas-menu' %}
            {% partial 'off-canvas-contact' %}
            {% partial 'off-canvas-language' %}
            
            <div class=\"off-canvas-content\" data-off-canvas-content>

                <!-- Page content -->
                {% page %}
                <!-- Contact callout -->
                {% partial 'contact-callout' %}
                <!-- Footer -->
                {% partial 'global/footer' %}

            </div>

        </div>

        <!-- Scripts -->
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.0/js/swiper.min.js\"></script>
        <script src=\"{{ 'assets/js/app.js'|theme }}\"></script>

    </body>
</html>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/layouts/default.htm", "");
    }
}
