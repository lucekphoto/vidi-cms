<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/partials/home-slider.htm */
class __TwigTemplate_8f5a881fa92da3b2cef2723ea0e5edeb26dd6a4f38a2c5e2367f6448b5545134 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"swiper-container\">
    <div class=\"swiper-wrapper\">
        <div class=\"swiper-slide\" style=\"background-image: url(";
        // line 3
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/hero-slider/hero-1.jpg");
        echo ")\">
            <div class=\"swiper-slide-inner swiper-slide-inner-graphic map\">
                <div class=\"grid-container grid-container--nomp\">
                    <div class=\"grid-x swiper-content-outer-wrapper\">
                        <div class=\"small-10 large-7 cell\">
                            <div class=\"short-spacer\"></div>
                            <h1 class=\"swiper-slide-title\">VIDI GROUP
                                <br>Więcej niż transport</h1>
                            <div class=\"swiper-slide-lead\">Jesteśmy agencją transportową, która przenosi relacje biznesowe na wyższy, dotąd nieznany w branży poziom.</div>
                            <div class=\"swiper-slide-btn-wrapper\">
                                <a href=\"#homeService\" class=\"button secondary swiper-slide-btn\" data-smooth-scroll>Sprawdź naszą ofertę</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"swiper-slide\" style=\"background-image: url(";
        // line 20
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/hero-slider/hero-2.jpg");
        echo ")\">
            <div class=\"swiper-slide-inner swiper-slide-inner-graphic bus\">
                <div class=\"grid-container grid-container--nomp\">
                    <div class=\"grid-x swiper-content-outer-wrapper\">
                        <div class=\"small-10 large-6 cell\">
                            <div class=\"short-spacer\"></div>
                            <h1 class=\"swiper-slide-title\">Nasze małe samochody dostawcze to Twoje duże możliwości.</h1>
                            <div class=\"swiper-slide-lead\">1500 km w 24h to nie jest wyzwanie to standard. Unia Europejska, Norwegia i Szwajcaria to tam właśnie jeździmy. Odbieramy towar w 2h od otrzymania zlecenia transportowego.</div>
                            <div class=\"swiper-slide-btn-wrapper\">
                                <a href=\"";
        // line 29
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("flota");
        echo "\" class=\"button secondary swiper-slide-btn\">Zobacz nasze samochody</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"swiper-slide\" style=\"background-image: url(";
        // line 36
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/hero-slider/hero-3.jpg");
        echo ")\">
            <div class=\"swiper-slide-inner swiper-slide-inner-graphic team\">
                <div class=\"grid-container\">
                    <div class=\"grid-x swiper-content-outer-wrapper\">
                        <div class=\"small-10 large-6 cell\">
                            <div class=\"short-spacer\"></div>
                            <h1 class=\"swiper-slide-title\">Nasza kadra to twój sukces</h1>
                            <div class=\"swiper-slide-lead\">Transport to nasza pasja. Zespół specjalistów dobierze dla Ciebie rozwiązanie szyte na miarę. To wszytko w 15 min i zupełnie bez zobowiązań</div>
                            <div class=\"swiper-slide-btn-wrapper\">
                                <a href=\"";
        // line 45
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("zespol");
        echo "\" class=\"button secondary swiper-slide-btn\">Zobacz nasz zespół</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Arrows -->
    <div class=\"swiper-button-next\"></div>
    <div class=\"swiper-button-prev\"></div>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/home-slider.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 45,  65 => 36,  55 => 29,  43 => 20,  23 => 3,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"swiper-container\">
    <div class=\"swiper-wrapper\">
        <div class=\"swiper-slide\" style=\"background-image: url({{ 'assets/img/hero-slider/hero-1.jpg'|theme }})\">
            <div class=\"swiper-slide-inner swiper-slide-inner-graphic map\">
                <div class=\"grid-container grid-container--nomp\">
                    <div class=\"grid-x swiper-content-outer-wrapper\">
                        <div class=\"small-10 large-7 cell\">
                            <div class=\"short-spacer\"></div>
                            <h1 class=\"swiper-slide-title\">VIDI GROUP
                                <br>Więcej niż transport</h1>
                            <div class=\"swiper-slide-lead\">Jesteśmy agencją transportową, która przenosi relacje biznesowe na wyższy, dotąd nieznany w branży poziom.</div>
                            <div class=\"swiper-slide-btn-wrapper\">
                                <a href=\"#homeService\" class=\"button secondary swiper-slide-btn\" data-smooth-scroll>Sprawdź naszą ofertę</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"swiper-slide\" style=\"background-image: url({{ 'assets/img/hero-slider/hero-2.jpg'|theme }})\">
            <div class=\"swiper-slide-inner swiper-slide-inner-graphic bus\">
                <div class=\"grid-container grid-container--nomp\">
                    <div class=\"grid-x swiper-content-outer-wrapper\">
                        <div class=\"small-10 large-6 cell\">
                            <div class=\"short-spacer\"></div>
                            <h1 class=\"swiper-slide-title\">Nasze małe samochody dostawcze to Twoje duże możliwości.</h1>
                            <div class=\"swiper-slide-lead\">1500 km w 24h to nie jest wyzwanie to standard. Unia Europejska, Norwegia i Szwajcaria to tam właśnie jeździmy. Odbieramy towar w 2h od otrzymania zlecenia transportowego.</div>
                            <div class=\"swiper-slide-btn-wrapper\">
                                <a href=\"{{ 'flota'|page }}\" class=\"button secondary swiper-slide-btn\">Zobacz nasze samochody</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"swiper-slide\" style=\"background-image: url({{ 'assets/img/hero-slider/hero-3.jpg'|theme }})\">
            <div class=\"swiper-slide-inner swiper-slide-inner-graphic team\">
                <div class=\"grid-container\">
                    <div class=\"grid-x swiper-content-outer-wrapper\">
                        <div class=\"small-10 large-6 cell\">
                            <div class=\"short-spacer\"></div>
                            <h1 class=\"swiper-slide-title\">Nasza kadra to twój sukces</h1>
                            <div class=\"swiper-slide-lead\">Transport to nasza pasja. Zespół specjalistów dobierze dla Ciebie rozwiązanie szyte na miarę. To wszytko w 15 min i zupełnie bez zobowiązań</div>
                            <div class=\"swiper-slide-btn-wrapper\">
                                <a href=\"{{ 'zespol'|page }}\" class=\"button secondary swiper-slide-btn\">Zobacz nasz zespół</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Arrows -->
    <div class=\"swiper-button-next\"></div>
    <div class=\"swiper-button-prev\"></div>
</div>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/partials/home-slider.htm", "");
    }
}
