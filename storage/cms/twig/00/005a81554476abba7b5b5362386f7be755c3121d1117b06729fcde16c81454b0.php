<?php

/* C:\Users\Luke\wamp\www\vidi-cms/themes/vidi-test/pages/home.htm */
class __TwigTemplate_912b6890be1f96b13eba9e019bd59a1ff0a9bb8541f510627ca1c4b4d55b4eda extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"grid-container full main-header\">

    <!-- Title bar only on \"small\" screens -->
    ";
        // line 4
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("title-bar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 5
        echo "    
    <div class=\"top-nav show-for-large\">

        <div class=\"grid-container\">
            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell\">

                    <!-- Info bar -->
                    ";
        // line 13
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("top-nav-info-bar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 14
        echo "
                    <!-- Main navigation -->
                    <div class=\"main-nav show-for-large\">
                        <div class=\"main-nav__logo\">
                            <a href=\"";
        // line 18
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("home");
        echo "\" class=\"main-nav__logo-link\">
                                <img src=\"";
        // line 19
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/vidi-group-logo-header.png");
        echo "\"
                                        srcset=\"";
        // line 20
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/vidi-group-logo-header.png");
        echo " 1x,
                                                ";
        // line 21
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/vidi-group-logo-header@2x.png");
        echo " 2x,
                                                ";
        // line 22
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/vidi-group-logo-header@3x.png");
        echo " 3x\" class=\"main-nav__logo-img\" alt=\"Vidi Group logotyp\">
                            </a>
                        </div>
                        <div class=\"main-nav__menu\">

                            <ul class=\"dropdown menu\" data-dropdown-menu>
                                <li><a href=\"";
        // line 28
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("home");
        echo "\">Strona główna</a></li>
                                <li class=\"is-dropdown-submenu-parent\">
                                    <a class=\"not-active\">Usługi</a>
                                    <ul class=\"menu vertical\">
                                        <li><a href=\"";
        // line 32
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-ekspresowy");
        echo "\">Transport ekspresowy</a></li>
                                        <li><a href=\"";
        // line 33
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-targi");
        echo "\">Transport na targi</a></li>
                                        <li><a href=\"";
        // line 34
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-przesiedlenia");
        echo "\">Transport przesiedlenia</a></li>
                                        <li><a href=\"";
        // line 35
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-chlodniczy");
        echo "\">Transport towarów chłodniczych</a></li>
                                        <li><a href=\"";
        // line 36
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-adr");
        echo "\">Transport towarów niebezpiecznych</a></li>
                                        <li><a href=\"";
        // line 37
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-na-wiszaco");
        echo "\">Transport towarów \"na wisząco\"</a></li>
                                    </ul>
                                </li>
                                <li><a href=\"";
        // line 40
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("flota");
        echo "\">Flota</a></li>
                                <li class=\"is-dropdown-submenu-parent\">
                                    <a href=\"";
        // line 42
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("o-firmie");
        echo "\">O firmie</a>
                                    <ul class=\"menu vertical\">
                                        <li><a href=\"";
        // line 44
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("zespol");
        echo "\">Team</a></li>
                                        <li><a href=\"";
        // line 45
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("dokumenty");
        echo "\">Dokumenty</a></li>
                                    </ul>
                                </li>
                                <li><a href=\"";
        // line 48
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("kontakt");
        echo "\">Kontakt</a></li>
                                
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- Home Slider - Swiper -->
    ";
        // line 60
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("home-slider"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 61
        echo "</div>
    
    <section class=\"section-full section-hp-service section-full--large\">
        <div class=\"grid-container\" id=\"homeService\">

            <div class=\"grid-x grid-margin-x align-middle align-center\">
                <div class=\"cell small-12 medium-10\">
                    <div class=\"short-spacer short-spacer--center\"></div>
                    <h2 class=\"section-title section-title-desc\">W czym możemy ci pomóc?</h2>
                    <p>Nie wahaj się, skontaktuj się z naszym specjalistą. Doradzimy oraz zrealizujemy każdy transport ekspresowy oraz dedykowany.</p>
                </div>
            </div>
            
            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell large-6\">
                    <div class=\"grid-x grid-margin-x\">
                        <div class=\"cell small-12 service-box\" style=\"\">
                            <a href=\"";
        // line 78
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-ekspresowy");
        echo "\" class=\"service-box__link\">
                                <img src=\"";
        // line 79
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/service-box/service-6.jpg");
        echo "\" class=\"service-box__img\" alt=\"\">
                                <div class=\"service-box-content\">
                                    <svg class=\"svg-icon service-box__icon\"><use xlink:href=\"#icon-logi-5\"></use></svg>
                                    <h2 class=\"service-box__title\">Transport <br>ekspresowy</h2>
                                    <button class=\"button hollow secondary\">Zobacz więcej</button>
                                </div>
                            </a>
                        </div>
                        <div class=\"cell small-12 medium-6 service-box service-box-sq\">
                            <a href=\"";
        // line 88
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-na-wiszaco");
        echo "\" class=\"service-box__link\">
                                <img src=\"";
        // line 89
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/service-box/service-3.jpg");
        echo "\" class=\"service-box__img\" alt=\"\">
                                <div class=\"service-box-content\">
                                    <svg class=\"svg-icon service-box__icon\"><use xlink:href=\"#icon-logi-7\"></use></svg>
                                    <h2 class=\"service-box__title\">Transport towarów<br>&quot;na wisząco&quot;</h2>
                                    <button class=\"button hollow secondary\">Zobacz więcej</button>
                                </div>
                            </a>
                        </div>
                        <div class=\"cell small-12 medium-6 service-box service-box-sq\">
                            <a href=\"";
        // line 98
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-chlodniczy");
        echo "\" class=\"service-box__link\">
                                <img src=\"";
        // line 99
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/service-box/service-1.jpg");
        echo "\" class=\"service-box__img\" alt=\"\">
                                <div class=\"service-box-content\">
                                    <svg class=\"svg-icon service-box__icon\"><use xlink:href=\"#icon-logi-9\"></use></svg>
                                    <h2 class=\"service-box__title\">Transport towarów <br>chłodniczych</h2>
                                    <button class=\"button hollow secondary\">Zobacz więcej</button>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class=\"cell large-6\">
                    <div class=\"grid-x grid-margin-x\">
                        <div class=\"cell small-12 medium-6 service-box service-box-sq\">
                            <a href=\"";
        // line 112
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-przesiedlenia");
        echo "\" class=\"service-box__link\">
                                <img src=\"";
        // line 113
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/service-box/service-2.jpg");
        echo "\" class=\"service-box__img\" alt=\"\">
                                <div class=\"service-box-content\">
                                    <svg class=\"svg-icon service-box__icon\"><use xlink:href=\"#icon-logi-1\"></use></svg>
                                    <h2 class=\"service-box__title\">Transport rzeczy przesiedlenia</h2>
                                    <button class=\"button hollow secondary\">Zobacz więcej</button>
                                </div>
                            </a>
                        </div>
                        <div class=\"cell small-12 medium-6 service-box service-box-sq\">
                            <a href=\"";
        // line 122
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-targi");
        echo "\" class=\"service-box__link\">
                                <img src=\"";
        // line 123
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/service-box/service-4.jpg");
        echo "\" class=\"service-box__img\" alt=\"\">
                                <div class=\"service-box-content\">
                                    <svg class=\"svg-icon service-box__icon\"><use xlink:href=\"#icon-logi-8\"></use></svg>
                                    <h2 class=\"service-box__title\">Transport towarów <br>na targi</h2>
                                    <button class=\"button hollow secondary\">Zobacz więcej</button>
                                </div>
                            </a>
                        </div>
                        <div class=\"cell small-12 service-box\" style=\"\">
                            <a href=\"";
        // line 132
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("transport-adr");
        echo "\" class=\"service-box__link\">
                                <img src=\"";
        // line 133
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/service-box/service-8.jpg");
        echo "\" class=\"service-box__img\" alt=\"\">
                                <div class=\"service-box-content\">
                                    <svg class=\"svg-icon service-box__icon\"><use xlink:href=\"#icon-logi-10\"></use></svg>
                                    <h2 class=\"service-box__title\">Transport towarów <br>niebezpiecznych</h2>
                                    <button class=\"button hollow secondary\">Zobacz więcej</button>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>

    <section class=\"section-fw section-fw--gray\" style=\"position: relative;\">
        <div class=\"grid-container fluid\">
            <div class=\"grid-x grid-padding-x align-right\">
                
                <div class=\"cell small-12 medium-8 large-6 align-middle flex-container\">
                    <div class=\"section-fw-content\">
                        <div class=\"grid-x grid-padding-x\">
                            <div class=\"cell small-12 medium-8 large-6 section-fw-content-copy left-side\">
                                <div class=\"short-spacer short-spacer--right\"></div>
                                <h2>Flota dedykowana dla Ciebie.</h2>
                                <p style=\"margin-bottom: 20px; margin-top: 10px;\">Nowoczesna flota samochodów i wyszkoleni kierowcy z doświadczeniem. Poznaj pełną specyfikację pojazdów, które są do Twojej dyspozycji.</p>
                                <a href=\"";
        // line 159
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("flota");
        echo "\" class=\"button secondary button--nomargin\">Zobacz naszą flotę</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"cell small-12 medium-4 large-6 section-fw-content-img\" style=\"background-image: url(";
        // line 165
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/flota-1.jpg");
        echo ");\"></div>

            </div>
        </div>
    </section>

    <section class=\"section-fw section-fw--gray\" style=\"position: relative;\">
        <div class=\"grid-container fluid\">
            <div class=\"grid-x grid-padding-x align-right\">
    
                <div class=\"cell small-12 medium-4 large-6 small-order-2 medium-order-1 section-fw-content-img\" style=\"background-image: url(";
        // line 175
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/flota-3.jpg");
        echo ");\"></div>
    
                <div class=\"cell small-12 medium-8 large-6 align-middle flex-container small-order-1 medium-order-2\">
                    <div class=\"section-fw-content\">
                        <div class=\"grid-x grid-padding-x align-right\">
                            <div class=\"cell small-12 medium-8 large-6 section-fw-content-copy right-side\">
    
                                <ul class=\"icon-list icon-list--checkbox-big\">
                                    <li class=\"icon-list__item\">
                                        <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                        Nowoczesność
                                    </li>
                                    <li class=\"icon-list__item\">
                                        <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                        Ekologia
                                    </li>
                                    <li class=\"icon-list__item\">
                                        <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                        Niezawodność
                                    </li>
                                    <li class=\"icon-list__item\">
                                        <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                        Bezpieczeństwo
                                    </li>
                                    <li class=\"icon-list__item icon-list__item--last\">
                                        <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                        Wydajność
                                    </li>
                                </ul>
    
                            </div>
                        </div>
                    </div>
                </div>
    
            </div>
        </div>
    </section>

    <section class=\"section-fw section-fw-parallax\" data-src=\"";
        // line 214
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/map-parallax.jpg");
        echo "\" data-parallax>
        <div class=\"grid-container flex-container\">
            <div class=\"grid-x grid-margin-x align-middle align-center\" >
                
                <div class=\"cell small-10 medium-8 text-center section-fw-parallax-content\">
                    <div class=\"short-spacer short-spacer--center\"></div>
                    <h2>Złote punkty europy w zasięgu twojej ręki</h2>
                    <p class=\"text-justify\">Właśnie patrzysz ma mapę Europy. Towar możemy odebrać z i dostarczyć do każdego państwa zaznaczonego złotym markerem. Znamy kluczowe punkty łączące państwa na mapie, wiemy skąd odpływają promy oraz wiemy jak dojechać na Sylt. Przejścia graniczne Szawajcarii nie są nam obce, wybieramy Como/Chiasso jadąc na Modenę. EU + NO + CH.</p>
                </div>

            </div>
        </div>
    </section>

    <section class=\"section-full section-hp-about\">
        <div class=\"grid-container section-full-image\" style=\"background-image: url(";
        // line 229
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/team1.png");
        echo ")\">
            <div class=\"short-spacer short-spacer--center\"></div>
            <h2 class=\"section-title\">Vidi Group, dołącz do najlepszych!</h2>
            <div class=\"grid-x grid-margin-x\">
                
                <div class=\"cell small-12 large-6 text-justify\">
                    <p>Jesteśmy agencją transportową zrzeszającą grupę najlepszych kontrahentów z zagranicy wraz
                    z najlepszymi przewoźnikami z Polski.</p>
                    <p>Połączenie to daje ogrom możliwości i korzyści, a sama agencja jest urzeczywistnieniem snów. W transporcie ekspresowym wyznaczamy
                    trendy. Współpracując z nami wiesz, że przyszłość leży u Twych stóp.</p>
                    <!-- <p><a href=\"/o-firmie.html\">Czytaj więcej</a></p> -->
                    <div class=\"button-wrapper\">
                        <a href=\"";
        // line 241
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("o-firmie");
        echo "\" class=\"button secondary\">O firmie</a>
                    </div>
                </div>

                <div class=\"cell small-12 large-6 text-justify\">
                    <p>Misja i wizja to jest coś co nas wyróżnia. Nie ma dla nas kompromisów, jesteśmy zobowiązani dostarczyć Ci najlepsze rozwiązanie z zakresu transportu ekspresowego, a to wszytko w przeciągu 15 min zupełnie bez zobowiązań. Sprawdź, przekonaj się.</p>
                    <!-- <p><a href=\"/zespol.html\">Zobacz nas zespół</a></p> -->
                    <div class=\"button-wrapper\">
                        <a href=\"";
        // line 249
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("zespol");
        echo "\" class=\"button secondary\">Zobacz nasz zespół</a>
                    </div>
                </div>

            </div>
        </div>
    </section>";
    }

    public function getTemplateName()
    {
        return "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  389 => 249,  378 => 241,  363 => 229,  345 => 214,  303 => 175,  290 => 165,  281 => 159,  252 => 133,  248 => 132,  236 => 123,  232 => 122,  220 => 113,  216 => 112,  200 => 99,  196 => 98,  184 => 89,  180 => 88,  168 => 79,  164 => 78,  145 => 61,  141 => 60,  126 => 48,  120 => 45,  116 => 44,  111 => 42,  106 => 40,  100 => 37,  96 => 36,  92 => 35,  88 => 34,  84 => 33,  80 => 32,  73 => 28,  64 => 22,  60 => 21,  56 => 20,  52 => 19,  48 => 18,  42 => 14,  38 => 13,  28 => 5,  24 => 4,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"grid-container full main-header\">

    <!-- Title bar only on \"small\" screens -->
    {% partial 'title-bar' %}
    
    <div class=\"top-nav show-for-large\">

        <div class=\"grid-container\">
            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell\">

                    <!-- Info bar -->
                    {% partial 'top-nav-info-bar' %}

                    <!-- Main navigation -->
                    <div class=\"main-nav show-for-large\">
                        <div class=\"main-nav__logo\">
                            <a href=\"{{ 'home'|page }}\" class=\"main-nav__logo-link\">
                                <img src=\"{{ 'assets/img/vidi-group-logo-header.png'|theme }}\"
                                        srcset=\"{{ 'assets/img/vidi-group-logo-header.png'|theme }} 1x,
                                                {{ 'assets/img/vidi-group-logo-header@2x.png'|theme }} 2x,
                                                {{ 'assets/img/vidi-group-logo-header@3x.png'|theme }} 3x\" class=\"main-nav__logo-img\" alt=\"Vidi Group logotyp\">
                            </a>
                        </div>
                        <div class=\"main-nav__menu\">

                            <ul class=\"dropdown menu\" data-dropdown-menu>
                                <li><a href=\"{{ 'home'|page }}\">Strona główna</a></li>
                                <li class=\"is-dropdown-submenu-parent\">
                                    <a class=\"not-active\">Usługi</a>
                                    <ul class=\"menu vertical\">
                                        <li><a href=\"{{ 'transport-ekspresowy'|page }}\">Transport ekspresowy</a></li>
                                        <li><a href=\"{{ 'transport-targi'|page }}\">Transport na targi</a></li>
                                        <li><a href=\"{{ 'transport-przesiedlenia'|page }}\">Transport przesiedlenia</a></li>
                                        <li><a href=\"{{ 'transport-chlodniczy'|page }}\">Transport towarów chłodniczych</a></li>
                                        <li><a href=\"{{ 'transport-adr'|page }}\">Transport towarów niebezpiecznych</a></li>
                                        <li><a href=\"{{ 'transport-na-wiszaco'|page }}\">Transport towarów \"na wisząco\"</a></li>
                                    </ul>
                                </li>
                                <li><a href=\"{{ 'flota'|page }}\">Flota</a></li>
                                <li class=\"is-dropdown-submenu-parent\">
                                    <a href=\"{{ 'o-firmie'|page }}\">O firmie</a>
                                    <ul class=\"menu vertical\">
                                        <li><a href=\"{{ 'zespol'|page }}\">Team</a></li>
                                        <li><a href=\"{{ 'dokumenty'|page }}\">Dokumenty</a></li>
                                    </ul>
                                </li>
                                <li><a href=\"{{ 'kontakt'|page }}\">Kontakt</a></li>
                                
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- Home Slider - Swiper -->
    {% partial 'home-slider' %}
</div>
    
    <section class=\"section-full section-hp-service section-full--large\">
        <div class=\"grid-container\" id=\"homeService\">

            <div class=\"grid-x grid-margin-x align-middle align-center\">
                <div class=\"cell small-12 medium-10\">
                    <div class=\"short-spacer short-spacer--center\"></div>
                    <h2 class=\"section-title section-title-desc\">W czym możemy ci pomóc?</h2>
                    <p>Nie wahaj się, skontaktuj się z naszym specjalistą. Doradzimy oraz zrealizujemy każdy transport ekspresowy oraz dedykowany.</p>
                </div>
            </div>
            
            <div class=\"grid-x grid-margin-x\">
                <div class=\"cell large-6\">
                    <div class=\"grid-x grid-margin-x\">
                        <div class=\"cell small-12 service-box\" style=\"\">
                            <a href=\"{{ 'transport-ekspresowy'|page }}\" class=\"service-box__link\">
                                <img src=\"{{ 'assets/img/service-box/service-6.jpg'|theme }}\" class=\"service-box__img\" alt=\"\">
                                <div class=\"service-box-content\">
                                    <svg class=\"svg-icon service-box__icon\"><use xlink:href=\"#icon-logi-5\"></use></svg>
                                    <h2 class=\"service-box__title\">Transport <br>ekspresowy</h2>
                                    <button class=\"button hollow secondary\">Zobacz więcej</button>
                                </div>
                            </a>
                        </div>
                        <div class=\"cell small-12 medium-6 service-box service-box-sq\">
                            <a href=\"{{ 'transport-na-wiszaco'|page }}\" class=\"service-box__link\">
                                <img src=\"{{ 'assets/img/service-box/service-3.jpg'|theme }}\" class=\"service-box__img\" alt=\"\">
                                <div class=\"service-box-content\">
                                    <svg class=\"svg-icon service-box__icon\"><use xlink:href=\"#icon-logi-7\"></use></svg>
                                    <h2 class=\"service-box__title\">Transport towarów<br>&quot;na wisząco&quot;</h2>
                                    <button class=\"button hollow secondary\">Zobacz więcej</button>
                                </div>
                            </a>
                        </div>
                        <div class=\"cell small-12 medium-6 service-box service-box-sq\">
                            <a href=\"{{ 'transport-chlodniczy'|page }}\" class=\"service-box__link\">
                                <img src=\"{{ 'assets/img/service-box/service-1.jpg'|theme }}\" class=\"service-box__img\" alt=\"\">
                                <div class=\"service-box-content\">
                                    <svg class=\"svg-icon service-box__icon\"><use xlink:href=\"#icon-logi-9\"></use></svg>
                                    <h2 class=\"service-box__title\">Transport towarów <br>chłodniczych</h2>
                                    <button class=\"button hollow secondary\">Zobacz więcej</button>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class=\"cell large-6\">
                    <div class=\"grid-x grid-margin-x\">
                        <div class=\"cell small-12 medium-6 service-box service-box-sq\">
                            <a href=\"{{ 'transport-przesiedlenia'|page }}\" class=\"service-box__link\">
                                <img src=\"{{ 'assets/img/service-box/service-2.jpg'|theme }}\" class=\"service-box__img\" alt=\"\">
                                <div class=\"service-box-content\">
                                    <svg class=\"svg-icon service-box__icon\"><use xlink:href=\"#icon-logi-1\"></use></svg>
                                    <h2 class=\"service-box__title\">Transport rzeczy przesiedlenia</h2>
                                    <button class=\"button hollow secondary\">Zobacz więcej</button>
                                </div>
                            </a>
                        </div>
                        <div class=\"cell small-12 medium-6 service-box service-box-sq\">
                            <a href=\"{{ 'transport-targi'|page }}\" class=\"service-box__link\">
                                <img src=\"{{ 'assets/img/service-box/service-4.jpg'|theme }}\" class=\"service-box__img\" alt=\"\">
                                <div class=\"service-box-content\">
                                    <svg class=\"svg-icon service-box__icon\"><use xlink:href=\"#icon-logi-8\"></use></svg>
                                    <h2 class=\"service-box__title\">Transport towarów <br>na targi</h2>
                                    <button class=\"button hollow secondary\">Zobacz więcej</button>
                                </div>
                            </a>
                        </div>
                        <div class=\"cell small-12 service-box\" style=\"\">
                            <a href=\"{{ 'transport-adr'|page }}\" class=\"service-box__link\">
                                <img src=\"{{ 'assets/img/service-box/service-8.jpg'|theme }}\" class=\"service-box__img\" alt=\"\">
                                <div class=\"service-box-content\">
                                    <svg class=\"svg-icon service-box__icon\"><use xlink:href=\"#icon-logi-10\"></use></svg>
                                    <h2 class=\"service-box__title\">Transport towarów <br>niebezpiecznych</h2>
                                    <button class=\"button hollow secondary\">Zobacz więcej</button>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>

    <section class=\"section-fw section-fw--gray\" style=\"position: relative;\">
        <div class=\"grid-container fluid\">
            <div class=\"grid-x grid-padding-x align-right\">
                
                <div class=\"cell small-12 medium-8 large-6 align-middle flex-container\">
                    <div class=\"section-fw-content\">
                        <div class=\"grid-x grid-padding-x\">
                            <div class=\"cell small-12 medium-8 large-6 section-fw-content-copy left-side\">
                                <div class=\"short-spacer short-spacer--right\"></div>
                                <h2>Flota dedykowana dla Ciebie.</h2>
                                <p style=\"margin-bottom: 20px; margin-top: 10px;\">Nowoczesna flota samochodów i wyszkoleni kierowcy z doświadczeniem. Poznaj pełną specyfikację pojazdów, które są do Twojej dyspozycji.</p>
                                <a href=\"{{ 'flota'|page }}\" class=\"button secondary button--nomargin\">Zobacz naszą flotę</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"cell small-12 medium-4 large-6 section-fw-content-img\" style=\"background-image: url({{ 'assets/img/flota-1.jpg'|theme }});\"></div>

            </div>
        </div>
    </section>

    <section class=\"section-fw section-fw--gray\" style=\"position: relative;\">
        <div class=\"grid-container fluid\">
            <div class=\"grid-x grid-padding-x align-right\">
    
                <div class=\"cell small-12 medium-4 large-6 small-order-2 medium-order-1 section-fw-content-img\" style=\"background-image: url({{ 'assets/img/flota-3.jpg'|theme }});\"></div>
    
                <div class=\"cell small-12 medium-8 large-6 align-middle flex-container small-order-1 medium-order-2\">
                    <div class=\"section-fw-content\">
                        <div class=\"grid-x grid-padding-x align-right\">
                            <div class=\"cell small-12 medium-8 large-6 section-fw-content-copy right-side\">
    
                                <ul class=\"icon-list icon-list--checkbox-big\">
                                    <li class=\"icon-list__item\">
                                        <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                        Nowoczesność
                                    </li>
                                    <li class=\"icon-list__item\">
                                        <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                        Ekologia
                                    </li>
                                    <li class=\"icon-list__item\">
                                        <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                        Niezawodność
                                    </li>
                                    <li class=\"icon-list__item\">
                                        <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                        Bezpieczeństwo
                                    </li>
                                    <li class=\"icon-list__item icon-list__item--last\">
                                        <svg class=\"svg-icon icon-list__icon icon-list__icon--secondary\"><use xlink:href=\"#icon-checkmark-circle-line\"></use></svg>
                                        Wydajność
                                    </li>
                                </ul>
    
                            </div>
                        </div>
                    </div>
                </div>
    
            </div>
        </div>
    </section>

    <section class=\"section-fw section-fw-parallax\" data-src=\"{{ 'assets/img/map-parallax.jpg'|theme }}\" data-parallax>
        <div class=\"grid-container flex-container\">
            <div class=\"grid-x grid-margin-x align-middle align-center\" >
                
                <div class=\"cell small-10 medium-8 text-center section-fw-parallax-content\">
                    <div class=\"short-spacer short-spacer--center\"></div>
                    <h2>Złote punkty europy w zasięgu twojej ręki</h2>
                    <p class=\"text-justify\">Właśnie patrzysz ma mapę Europy. Towar możemy odebrać z i dostarczyć do każdego państwa zaznaczonego złotym markerem. Znamy kluczowe punkty łączące państwa na mapie, wiemy skąd odpływają promy oraz wiemy jak dojechać na Sylt. Przejścia graniczne Szawajcarii nie są nam obce, wybieramy Como/Chiasso jadąc na Modenę. EU + NO + CH.</p>
                </div>

            </div>
        </div>
    </section>

    <section class=\"section-full section-hp-about\">
        <div class=\"grid-container section-full-image\" style=\"background-image: url({{ 'assets/img/team1.png'|theme }})\">
            <div class=\"short-spacer short-spacer--center\"></div>
            <h2 class=\"section-title\">Vidi Group, dołącz do najlepszych!</h2>
            <div class=\"grid-x grid-margin-x\">
                
                <div class=\"cell small-12 large-6 text-justify\">
                    <p>Jesteśmy agencją transportową zrzeszającą grupę najlepszych kontrahentów z zagranicy wraz
                    z najlepszymi przewoźnikami z Polski.</p>
                    <p>Połączenie to daje ogrom możliwości i korzyści, a sama agencja jest urzeczywistnieniem snów. W transporcie ekspresowym wyznaczamy
                    trendy. Współpracując z nami wiesz, że przyszłość leży u Twych stóp.</p>
                    <!-- <p><a href=\"/o-firmie.html\">Czytaj więcej</a></p> -->
                    <div class=\"button-wrapper\">
                        <a href=\"{{ 'o-firmie'|page }}\" class=\"button secondary\">O firmie</a>
                    </div>
                </div>

                <div class=\"cell small-12 large-6 text-justify\">
                    <p>Misja i wizja to jest coś co nas wyróżnia. Nie ma dla nas kompromisów, jesteśmy zobowiązani dostarczyć Ci najlepsze rozwiązanie z zakresu transportu ekspresowego, a to wszytko w przeciągu 15 min zupełnie bez zobowiązań. Sprawdź, przekonaj się.</p>
                    <!-- <p><a href=\"/zespol.html\">Zobacz nas zespół</a></p> -->
                    <div class=\"button-wrapper\">
                        <a href=\"{{ 'zespol'|page }}\" class=\"button secondary\">Zobacz nasz zespół</a>
                    </div>
                </div>

            </div>
        </div>
    </section>", "C:\\Users\\Luke\\wamp\\www\\vidi-cms/themes/vidi-test/pages/home.htm", "");
    }
}
